'use strict';

var gulp = require('gulp');
var path = require('path');
var conf = require('./conf');
var replace = require('gulp-replace');

gulp.task('replace-string', function(){
  gulp.src([path.join(conf.paths.src, '/**/*.{js,html,css,scss}')])
    .pipe(replace('BlurAdmin', 'Proelio'))
    .pipe(gulp.dest(conf.paths.src));
});
