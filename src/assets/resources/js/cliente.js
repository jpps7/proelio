$(document).on("ready", inicio);
var canvas = null;
var videocanvas = null;
var lienzoActivo = 0;
var buttonWait = 5;
var timeWaited = buttonWait;
var alreadyPlayed = false;
var vcoordx, vcoordy, vwidht, vheight;
var hasVideo = false;
var device;

function getUrlParameter(sParam)
{
    var sPageURL = decodeURIComponent(window.location.search.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function bloquearUI( e )
{
    if( e )
    {
        $("#loader").addClass("active");
    }
    else
    {
        $("#loader").removeClass("active");
    }
}

function breadcrumb( e )
{
    var idmenu = $(this).data("idmenu");
    var index = $(this).index();
    var raiz = $(this).data("raiz");

    if( raiz > 0 ) index = 0;
    if( index > 0 ) index--;
    $("#springboard .breadcrumb li:gt(" + index + ")").remove();

    obtener_menu.call( this, e );
    seleccionar_menu.call( this, e )
    
    if( e != null )
        e.preventDefault();
}

function buscar_lienzo( e )
{
    var listado = e.data;
    var clone_breadcrumb = $("#springboard .breadcrumb").clone();
    $( clone_breadcrumb ).find("li:gt(0)").remove();
    $("#springboard").empty();

    $("<li/>")
        .attr("data-idmenu", 0)
        .attr("data-opcion", "menu")
        .text( "Búsqueda" )
        .appendTo( clone_breadcrumb );
    $( clone_breadcrumb )
        .find("li")
        .on("click", breadcrumb);
    $(clone_breadcrumb)
        .addClass("active")
        .appendTo("#springboard");

    $.ajax
        ({
            type: "GET",
            url: "ws/lienzos/buscar",
            dataType: "json",
            data: $(this).serialize(),
            success: 
            function( result )
            {
                bloquearUI( false );
                //console.log( result );

                if( result.resultado )
                {
                    var listado = result.registros;

                    for( x in listado )
                    {
                        var item = listado[x];
                        var lienzo =
                        $("<div/>")
                            .addClass("sketch")
                            .attr("data-archivo", item.archivo)
                            .attr("data-idmenu", item.id)
                            .attr("data-nombre", item.nombre)
                            .attr("data-calificacion", item.calificacion)
                            .attr("data-opcion", "lienzo")
                            .on("click", obtener_lienzo)
                            .appendTo( "#springboard" );

                        $("<div/>")
                            .addClass("photo")
                            .css("background-image", "url(resources/images/Tigo-Azul.png)")
                            .css("background-size", "contain")
                            .css("background-position-y", "center")
                            .css("background-repeat", "no-repeat")
                            .appendTo( lienzo );

                        $("<div/>")
                            .addClass("text")
                            .append( "<div class='title'>" + item.nombre + "</div>" )
                            .append( "<div class=''>" + item.descripcion + "</div>" )
                            .append( "<div class='date'>" + item.created_at + "</div>" )
                            .appendTo( lienzo );

                        var stars =
                        $("<div/>")
                            .addClass("stars")
                            .appendTo( lienzo );

                        // CONTADOR DE ESTRELLAS (VALORACION)
                        var nstars_filled   = item.calificacion; // OBTENER DATO DEL WEBSERVICE
                        var nstars_outline  = 5 - nstars_filled;

                        for( i = 0; i < nstars_filled; i++ )
                        {
                            $("<i/>")
                                .addClass("icon-star")
                                .appendTo( stars );
                        }
                        for( j = 0; j < nstars_outline; j++ )
                        {
                            $("<i/>")
                                .addClass("icon-star-outline")
                                .appendTo( stars );
                        }
                    }
                }
                else
                {
                    //console.log("FALSE");
                }
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    //console.log("Error");
                    //console.error( err );
            }
        });

    if( e != null )
        e.preventDefault();
}

function enviar_calificacion( e )
{
    var id              = $(this).data("id");
    var index           = $(this).index() - 1;
    var calificacion    = $(this).index();
    var padre           = $(this).parent();

    $( padre )
        .find("i:gt(" + index + ")")
        .addClass("icon-star-outline")
        .removeClass("icon-star");
    $( padre )
        .find("i:lt(" + index + ")")
        .addClass("icon-star")
        .removeClass("icon-star-outline");
    $( this )
        .addClass("icon-star")
        .removeClass("icon-star-outline");

    bloquearUI( true );

    $.ajax
        ({
            type: "POST",
            url: "ws/cliente/calificacion",
            dataType: "json",
            data: { usuario: $("#objNofind").val(), lienzo: id, calificacion: calificacion },
            success: 
            function( result )
            {
                bloquearUI( false );
                //console.log( result );
                var lienzo = $("<div/>").addClass("iframe");
                $("<div/>")
                    .addClass("warning")
                    .text(result.mensaje)
                    .appendTo("#notifications");

                setTimeout("$('#notifications').empty()", 5000);

                if( result.resultado )
                {
                    //console.log(result.registros);
                    $('.valoration .total').html(result.calificacion);
                }
                else
                {
                    //console.log("FALSE");
                }
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    //console.log("Error");
                    //console.error( err );
            }
        });
}

function enviar_encuesta( id )
{
    var datos       = Object();
    datos.encuesta  = Array();
    datos.id        = id;
    var userSend        = $("#objNofind").val();
    
    bloquearUI( true );

    $("#myquiz").find(".question").each( function( index, element )
    {
        var pregunta    = Object();

        pregunta.id     = $(element).data("id");
        pregunta.tipo   = $(element).data("tipo");

        switch( pregunta.tipo )
        {
            case 1: // CERRADA
            {
                var respuesta               = $( element ).find("input:checked");
                
                pregunta.respuesta          = Object();
                pregunta.respuesta.id       = $(respuesta).val();
                pregunta.respuesta.value    = $(respuesta).val();
            } break;
            case 2: // MULTIPLE
            {
                var respuestas = Array();

                $( element ).find("input:checked").each( function( index2, element2 ) 
                {
                    var respuesta   = Object();

                    respuesta.id    = $(element2).val();
                    respuesta.value = $(element2).val();

                    respuestas.push( respuesta );
                });

                pregunta.respuesta  = respuestas;
            } break;
            case 3: // ABIERTA
            {
                var respuesta               = $( element ).find("textarea");

                pregunta.respuesta          = Object();
                pregunta.respuesta.id       = null;
                pregunta.respuesta.value    = $( respuesta ).val();
            }
        }

        datos.encuesta.push( pregunta );
    });

    //console.log(  datos );
    
    $.ajax
        ({
            type: "POST",
            url: "ws/cliente/respuestas",
            dataType: "json",
            data: { usuario: userSend, respuesta: JSON.stringify(datos) },
            success: 
            function( result )
            {
                bloquearUI( false );
                //console.log( result );

                if( result.resultado )
                {
                    mostrar_popup( null, result.registros.valor, "¡Encuesta Completada!" );
                    //obtener_menu( null );
                    obtener_encuestas( null );
                    //$(".btn-encuestas").click();
                    $(".breadcrumb li:first").click();
                    //console.log(result.registros);
                }
                else
                {
                    //console.log("FALSE");
                }
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    //console.log("Error");
                    //console.error( err );
            }
        });
}

function enviar_sugerencia( e )
{
    var id              = lienzoActivo;
    var sugerencia      = $("#sugerencia textarea").val();
    var userSend        = $("#objNofind").val();
    if (!sugerencia) {
        alert('Debe ingresar una sugerencia.');
        return;
    }
    
    if (!userSend) {
        alert('Debe estar logeado para ingresar una sugerencia.');
        return;
    }

    bloquearUI( true );

    $.ajax
        ({
            type: "POST",
            url: "ws/cliente/sugerencia",
            dataType: "json",
            data: { usuario: userSend, lienzo: id, texto_sugerencia: sugerencia },
            success: 
            function( result )
            {
                bloquearUI( false );
                //console.log( result );

                if( result.resultado )
                {
                    if (typeof result.registros.notemp !== 'undefined' && result.registros.notemp) {
                        mostrar_popup( null, false, "¡Sugerencia Enviada!" );
                    } else {
                        mostrar_popup( null, result.registros.valor, "¡Sugerencia Enviada!" );
                    }
                    $("#sugerencia").trigger("click");
                }
                else
                {
                    //console.log("FALSE");
                }
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    //console.log("Error");
                    //console.error( err );
            }
        });
}

function inicio( e )
{
    var idlienzo = getUrlParameter("id");
    
    $("#btn-menu").on("click", mostrar_menu);
    $("#btn-search").on("click", mostrar_busqueda);
    $("#myquiz")
        .find(".next")
        .on("click", quiz_siguiente_pregunta);
    $("#popup").on("click", mostrar_popup);
    $("#sugerencia").on("click", mostrar_sugerencia);
    $("#sugerencia button").on("click", enviar_sugerencia);
    $(".searchbar > form").on("submit", buscar_lienzo);
    $("li.logout").on("click", salirSistema);
    $("li.admin").on("click", irAlAdministrador);

    if( $("#objTypeNo").val() < 1 )
    {
        $("li.admin").remove();
    }
    
    obtener_menu( null );
    obtener_encuestas( null );
    
    // SI ES EDICION DE LIENZO, CARGAMOS LA DATA
    if( idlienzo > 0 )
    {
        $.ajax
        ({
            type: "GET",
            url: "ws/lienzos/" + idlienzo,
            dataType: "json",
            beforeSend: function(x) {
                if(x && x.overrideMimeType) {
                 x.overrideMimeType("application/j-son;charset=ISO-8859-1");
               }},
            data: {},
            success: 
            function( result )
            {
                mostrar_lienzo( result.registros );
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    console.error( err );
            }
        });
    }
    
    gameLoop();
    
    /*calculando();

    function calculando(){
      var height = $(window).height();

      $(".btn-sugerencia").css({
        top: (height - 100) + "px",
      });
    }*/
}

function irAlAdministrador( e )
{
    window.location = "panel";
}

function llenar_categoria( listado, nodo )
{
    var clone_breadcrumb = $("#springboard .breadcrumb").clone();
    $("#springboard").empty();

    $("<li/>")
        .attr("data-idmenu", nodo.idmenu)
        .attr("data-opcion", "menu")
        .text( nodo.texto )
        .appendTo( clone_breadcrumb );
    $( clone_breadcrumb )
        .find("li")
        .on("click", breadcrumb);
    $(clone_breadcrumb)
        .addClass("active")
        .appendTo("#springboard");

    for( x in listado )
    {
        var item = listado[x];
        var lienzo =
        $("<div/>")
            .addClass("sketch")
            .attr("data-archivo", item.archivo)
            .attr("data-idmenu", item.id)
            .attr("data-nombre", item.nombre)
            .attr("data-calificacion", item.calificacion)
            .attr("data-opcion", "lienzo")
            .on("click", obtener_lienzo)
            .appendTo( "#springboard" );

        $("<div/>")
            .addClass("photo")
            .css("background-image", "url(resources/images/Tigo-Azul.png)")
            .css("background-size", "contain")
            .css("background-position-y", "center")
            .css("background-repeat", "no-repeat")
            .appendTo( lienzo );

        $("<div/>")
            .addClass("text")
            .append( "<div class='title'>" + item.nombre + "</div>" )
            .append( "<div class=''>" + item.descripcion + "</div>" )
            .append( "<div class='date'>" + item.created_at + "</div>" )
            .appendTo( lienzo );

        var stars =
        $("<div/>")
            .addClass("stars")
            .appendTo( lienzo );

        // CONTADOR DE ESTRELLAS (VALORACION)
        var nstars_filled   = item.calificacion; // OBTENER DATO DEL WEBSERVICE
        var nstars_outline  = 5 - nstars_filled;
        
        $("<span/>")
                .addClass("show-stars")
                .append(item.calificacion)
                .appendTo( stars );
        
        $("<i/>")
                .addClass("icon-star")
                .addClass("show-stars")
                .appendTo( stars );

        /*for( i = 0; i < nstars_filled; i++ )
        {
            $("<i/>")
                .addClass("icon-star")
                .appendTo( stars );
        }*/
        /*for( j = 0; j < nstars_outline; j++ )
        {
            $("<i/>")
                .addClass("icon-star-outline")
                .appendTo( stars );
        }*/
    }
}

function llenar_encuestas( e )
{
    var listado = e.data;
    var clone_breadcrumb = $("#springboard .breadcrumb").clone();
    $( clone_breadcrumb ).find("li:gt(0)").remove();
    $("#springboard").empty();

    $("<li/>")
        .attr("data-idmenu", 0)
        .attr("data-opcion", "menu")
        .text( "Encuestas" )
        .appendTo( clone_breadcrumb );
    $( clone_breadcrumb )
        .find("li")
        .on("click", breadcrumb);
    $(clone_breadcrumb)
        .addClass("active")
        .appendTo("#springboard");
    
    for( x in listado )
    {
        var item = listado[x];
        var encuesta =
        $("<div/>")
            .addClass("sketch")
            .attr("data-opcion", "encuestas")
            .on("click", item, mostrar_encuesta)
            .appendTo( "#springboard" );

        $("<div/>")
            .addClass("photo")
            .css("background-image", "url(resources/images/Tigo-Azul.png)")
            .css("background-size", "contain")
            .css("background-position-y", "center")
            .css("background-repeat", "no-repeat")
            .appendTo( encuesta );

        $("<div/>")
            .addClass("text")
            .append( "<div class='title'>" + item.nombre + "</div>" )
            .append( "<div class=''>" + item.descripcion + "</div>" )
            .append( "<div class='date'>" + item.created_at + "</div>" )
            .appendTo( encuesta );
    }

    seleccionar_menu.call( this, e )
    
    if( e != null )
        e.preventDefault();
}

function llenar_menu( listado, nodo, tipo )
{
    console.log(listado);
    var clone_breadcrumb = $("#springboard .breadcrumb").clone();
    $("#springboard").empty();

    $(clone_breadcrumb).appendTo("#springboard");
    if( tipo > 0 )
    {
        $("<li/>")
            .attr("data-idmenu", nodo.idmenu)
            .attr("data-opcion", "menu")
            .text( nodo.texto )
            .appendTo( clone_breadcrumb );

        $( clone_breadcrumb )
            .find("li")
            .on("click", breadcrumb);

        $(clone_breadcrumb).addClass("active");
    }
    else
    {
        $(clone_breadcrumb).removeClass("active");
    }

    for( x in listado )
    {
        var item = listado[x];
        var menuButton = $("<div class='icon blue'></div>");
        if (typeof item.background !== 'undefined') {
            menuButton.attr("style", "background-color: " + item.background);
        }
        var boton =
        $("<div/>")
            .addClass("button")
            .append(menuButton)
            .attr("data-idmenu", item.id)
            .attr("data-opcion", "menu")
            .on("click", obtener_menu)
            .appendTo( "#springboard" );

        $("<img/>")
            .attr("src", item.icono)
            .appendTo( $(boton).find(".icon") );

        $("<div/>")
            .addClass("text blue")
            .text( item.nombre )
            .appendTo( boton );
    }

    if( nodo.idmenu === undefined || nodo.idmenu == 0 )
    {
        $("nav > ul > li.menu").remove();

        var selector = $("#objTypeNo").val()>0? "nav > ul > li.admin"
                        : "nav > ul > li.logout";

        for( x in listado )
        {
            var item = listado[x];
            var boton =
            $("<li/>")
                .addClass("menu")
                .attr("data-idmenu", item.id)
                .attr("data-opcion", "menu")
                .attr("data-raiz", 1)
                .on("click", breadcrumb)
                .text( item.nombre )
                .insertBefore( selector );
        }
    }
}

function mostrar_busqueda( e )
{
    if( $("#searchbar").hasClass("active") )
    {
        $("#searchbar").removeClass("active");
    }
    else
    {
        $("#searchbar")
            .addClass("active")
            .find("input")
            .focus();
    }

    if( e != null )
        e.preventDefault();
}

function mostrar_encuesta( e )
{
    var datos = e.data;
    var preguntas = datos.preguntas;
    var formulario = $("#myquiz form").empty();

    $("#myquiz")
        .find(".next")
        .attr("data-id", datos.id)
        .text("Siguiente Pregunta");
    $("#myquiz h2").text( datos.nombre );
    
    for( x in preguntas )
    {
        var pregunta = preguntas[x];
        var contador = parseInt(x)+1;
        var question =
        $("<div/>")
            .addClass("question")
            .attr("data-id", pregunta.id)
            .attr("data-tipo", pregunta.tipo)
            .append("<span class='number'>" + contador + "/" + preguntas.length + "</span>")
            .append("<div class='title'>" + pregunta.pregunta + "</div>")
            .append("<div class='reply'></div>")
            .appendTo( formulario );
        
        switch( pregunta.tipo )
        {
            case 1: // CERRADA
            {
                for( y in pregunta.respuestas )
                {
                    var respuesta = pregunta.respuestas[y];
                    var reply =
                    $("<div/>")
                        .addClass("radio")
                        .append("<label></label>")
                            .find("label")
                            .append("<input type='radio' name='pregunta_" + pregunta.id + "' value='" + respuesta.id + "' />")
                            .append("<span>" + respuesta.respuesta + "</span>")
                            .end()
                        .appendTo( $(question).find(".reply") );
                }
            }; break;
            case 2: // MULTIPLE
            {
                for( y in pregunta.respuestas )
                {
                    var respuesta = pregunta.respuestas[y];
                    var reply =
                    $("<div/>")
                        .addClass("checkbox")
                        .append("<label></label>")
                            .find("label")
                            .append("<input type='checkbox' name='pregunta_" + pregunta.id + "' value='" + respuesta.id + "' />")
                            .append("<span>" + respuesta.respuesta + "</span>")
                            .end()
                        .appendTo( $(question).find(".reply") );
                }
            }; break;
            case 3: // ABIERTA
            {
                $("<textarea/>")
                    .addClass("text")
                    .attr("placeholder", "Escribe aquí tu respuesta")
                    .appendTo( $(question).find(".reply") );
            }; break;
        }
    }

    seleccionar_menu.call( this, e )
    
    if( e != null )
        e.preventDefault();
}

function reproducir_video( e )
{
    if ( $(this).hasClass("active") ) 
    {
        $("video").each(function(key, value)
        {
            $(this)[0].stop();
        });
        $(this).removeClass("active")
    } 
    else
    {
        $("video").each(function(key, value)
        {
            $(this)[0].play();
        });
        $(this).addClass("active")
    }

    if( e != null )
        e.preventDefault();
}

function mostrar_lienzo( datos )
{
    hasVideo = false;
    var clone_breadcrumb = $("#springboard .breadcrumb").clone();
    var lienzo = $("<div/>").addClass("iframe");
    var valoracion = $("<div/>").addClass("valoration");
    $("#springboard").empty();

    $( clone_breadcrumb )
        .find("li")
        .on("click", breadcrumb);
    $(clone_breadcrumb)
        .addClass("active")
        .appendTo("#springboard");
    $(lienzo).append("<div class='sketch-htmlobjects' id='canvas-htmlobjects'></div>");
    var ulCanvas = $("<ul/>").addClass("ulCanvas");
    var liCanvasImg = $('<li/>').addClass('liCanvas');
    ulCanvas.append(liCanvasImg);
    var liCanvas = $("<li/>").addClass("liCanvas").append("<canvas id='mycanvas' width='" + datos.ancho + "' height='" + datos.alto + "'></canvas>");
    ulCanvas.append(liCanvas);
    var liVideo = $("<li/>").addClass("liCanvas").append("<canvas id='videocanvas' width='" + datos.ancho + "' height='" + datos.alto + "'></canvas>");
    ulCanvas.append(liVideo);
    $(lienzo).append(ulCanvas);
    
    $(lienzo).appendTo( "#springboard" );

    if( datos.htmlobjects.length > 0 )
    {
        var videosCanvas = [];
        if (datos.htmlobjects.indexOf("video") > 0) {
                hasVideo = true;
        }
        $.each(JSON.parse(datos.config).objects, function (k, v) {
            if (v.type === 'image' && (typeof v.src === 'undefined') || v.src === "") {
                //console.log(v);
                //videosCanvas.push(v);
                vcoordx = v.left;
                vcoordy = v.top;
                vwidth = v.width;
                vheight = v.height;
            }
        });
        
        var isMobile = { 
            Android: function() { return navigator.userAgent.match(/Android/i); }, 
            BlackBerry: function() { return navigator.userAgent.match(/BlackBerry/i); }, 
            iOS: function() { return navigator.userAgent.match(/iPhone|iPad|iPod/i); }, 
            Opera: function() { return navigator.userAgent.match(/Opera Mini/i); }, 
            Windows: function() { return navigator.userAgent.match(/IEMobile/i); }, 
            any: function() { return (isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows()); } };
        
        device = (isMobile.any() ? 'celular' : 'computadora');
        // verificar si hay video y determinar el dispositivo
        if (hasVideo) {
            $("<div/>")
                .addClass("warning")
                .text("Si tu " + device + " no reproduce el video automáticamente, dale click al botón de Reproducir video " + (device === 'celular' ? 'que aparece sobre el video' : ' que aparece arriba') + ".")
                .appendTo("#notifications");

            /*$("<button/>")
                .addClass("sketch-play-video")
                .on("click", reproducir_video)
                .appendTo(lienzo);*/
            setTimeout("$('#notifications').empty()", 5000);
        }
    }

    lienzoActivo = datos.id;
    //console.log("DATOS");
    //console.log(datos);

    /*
    $(lienzo)
        .load( datos.archivo, function()
        {
            $(this)
                .find("svg")
                .attr("height", "100%")
                .attr("width", "100%")
                [0].setAttribute("viewBox", "0 0 700 700");
        })
        .appendTo( "#springboard" );
    */

    // Canvas configuration
    canvas = this.__canvas = new fabric.StaticCanvas("mycanvas");
    var videoObject;
    $.each(JSON.parse(datos.config).objects, function () {
        if (this.type === 'image' && (typeof this.src === 'undefined' || this.src === "")) {
            videoObject = {
                objects: [this]
            };
        }
    });
    canvas.loadFromJSON(JSON.stringify(videoObject), function () {
        canvas.renderAll();
        $("#canvas-htmlobjects").html( datos.htmlobjects );

        canvas.setDimensions( { width: datos.ancho, height: datos.alto } );
    });
    
    /*canvas.loadFromJSON( datos.config, function()
    {
        canvas.renderAll();
        
        // Search and destroy empty elements!
        var objects = canvas.getObjects();
        //console.log(objects);
//        $.each(objects, function()
//        {
//            //if( this.type !== "image" || (this.type === 'image' && this.src !== ""))
//            if (!(this.type === 'image' && (typeof this.src === 'undefined' || this.src === "")))
//            {
//                console.log(this.type);
//                canvas.remove( this );
//            }
//        });
        
//        $.each(objects, function()
//        {
//            console.log(this);
//            if ((this.type === 'image' && this.src !== "") || this.type === 'rect' || this.type === 'circle' || this.type === 'triangle' || this.type === 'textbox')
//            {
//                canvas.remove( this );
//            }
//        });
//        console.log(objects);
        $("#canvas-htmlobjects").html( datos.htmlobjects );

        canvas.setDimensions( { width: datos.ancho, height: datos.alto } );
        //canvas.renderAll();
    });*/
    
    if (hasVideo) {
        videocanvas = document.getElementById('videocanvas');
        
        drawScreen();
        
        if (device === 'computadora') {
            videocanvas.addEventListener("mouseup", videocontrolsEvent, false);
        } else {
            videocanvas.addEventListener("touchend", videocontrolsTouchEvent, false);
        }
    }
    
    var imageCanvas = new Image();
    imageCanvas.src = datos.archivoImagen;
    $('.liCanvas:first').append(imageCanvas);
    
    // SUGERENCIAS
    $("#sugerencia button").attr("data-id", datos.id)
    $("<button/>")
        .addClass("btn-sugerencia")
        .on("click", mostrar_sugerencia)
        .text("Agregar una Sugerencia")
        .appendTo( "#springboard" );

    // CALIFICACION
    $("<div/>")
        .addClass("total")
        .text( datos.calificacion? datos.calificacion : 0 )
        .appendTo( valoracion );
    var stars =
    $("<div/>")
        .addClass("stars")
        .append("<label>¿Te ha resultado útil?</label>")
        .appendTo( valoracion );
    $(valoracion).appendTo( "#springboard" );

    // CONTADOR DE ESTRELLAS (VALORACION)
    //console.log(datos);
    // obtener la calificacion del lienzo realizada por el usuario
    $.ajax({
        type: "GET",
        url: "ws/cliente/calificacion/" + lienzoActivo,
        dataType: "json",
        data: {'uid': $("#objNofind").val()},
        success: function( result ) {
            var nstars_filled = 0;
            var nstars_outline = 5;
        
            if (result && result.resultado) {
                nstars_filled = result.calificacion; // OBTENER DATO DEL WEBSERVICE
                nstars_outline = 5 - nstars_filled;
            }
            for( i = 0; i < nstars_filled; i++ )
            {
                $("<i/>")
                    .addClass("icon-star")
                    .attr("data-opcion-id", datos.id)
                    .on("click", enviar_calificacion)
                    .appendTo( stars );
            }
            for( j = 0; j < nstars_outline; j++ )
            {
                $("<i/>")
                    .addClass("icon-star-outline")
                    .attr("data-id", datos.id)
                    .on("click", enviar_calificacion)
                    .appendTo( stars );
            }
        }
    });
    


    //$(lienzo)
    //	.css("content", "url("+datos.archivo+")")
    //	.appendTo( "#springboard" );
}

function mostrar_menu( e )
{
    if( $("body > nav").hasClass("active") )
    {
        $("body > nav").removeClass("active");
    }
    else
    {
        $("body > nav").addClass("active");
    }

    if( e != null )
        e.preventDefault();
}

function mostrar_sugerencia( e )
{
    //console.log( e );

    if( e == null )
    {
        $("#sugerencia").addClass("active");
        $("#sugerencia textarea").focus();
    }
    else if( $(this).hasClass("active") && e.target.nodeName != "BUTTON" )
    {
        if (e.target.type !== 'textarea') {
            $(".blurred").removeClass("blurred");
            $(this).removeClass("active");
            $("#sugerencia button").attr("data-id", "0");
            $("#sugerencia textarea").val("");
        }
    }
    else
    {
        mostrar_sugerencia( null );
    }

    if( e != null )
        e.preventDefault();
}

function mostrar_popup( e, valor, texto )
{
    if (!valor) {
        $('#popup').find('.min').text("Gracias por tu sugerencia!");
        $("#valor-puntos").text('');
    } else {
        $("#valor-puntos").text("¡"+ valor +" Puntos !");
    }
    $("#boton-pop-text").text(texto);
    if( e == null )
    {
        $("#popup").addClass("active");
    }
    else if( $(this).hasClass("active") )
    {
        $(".blurred").removeClass("blurred");
        $(this).removeClass("active");
    }
    else
    {
        mostrar_popup( null );
    }

    if( e != null )
        e.preventDefault();
}

function obtener_encuestas( e )
{
    var idusuario = $("#objNofind").val();;
    $("#btn-menu .badge").remove();
    
    $.ajax
        ({
            type: "GET",
            url: "ws/encuestas/usuario/" + idusuario,
            dataType: "json",
            data: {},
            beforeSend: function(x) {
                    if(x && x.overrideMimeType) {
                     x.overrideMimeType("application/j-son;charset=ISO-8859-1");
                   }},
            success: 
            function( result )
            {
                bloquearUI( false );
                
                if( result.resultado )
                {
                    var total = 0;
                    for (var i = 0; i < result.registros.length; i++) {
                        if (result.registros[i].estado === 0) {
                            total++;
                        }
                    }
                    if( total > 0 )
                    {
                        var boton = $(".btn-encuestas");

                        if( boton.length == 0 )
                        {
                            boton =
                            $("<li/>")
                                .text("Encuestas")
                                .addClass("btn-encuestas")
                                .append("<span class='badge'>" + total  + "</span>")
                                .attr("data-opcion", "menu")
                                .prependTo( "nav > ul" );
                        }

                        $("<i/>")
                            .addClass("badge")
                            .text( total )
                            .appendTo("#btn-menu");

                        $(boton).on("click", result.registros, llenar_encuestas);
                        $(boton).find(".badge").text( total );
                    } else {
                        $('.btn-encuestas').remove();
                    }
                }
                else
                {

                }
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    console.error( err );
            }
        });
        
    if( e != null )
        e.preventDefault();
}

function obtener_lienzo( e )
{
    var datos           = Object();
    datos.archivo       = $(this).data("archivo");
    datos.idmenu        = $(this).data("idmenu");
    datos.calificacion  = $(this).data("calificacion");

    $.ajax
        ({
            type: "GET",
            url: "ws/lienzos/" + datos.idmenu,
            dataType: "json",
            beforeSend: function(x) {
                            if(x && x.overrideMimeType) {
                             x.overrideMimeType("application/j-son;charset=ISO-8859-1");
                           }},
            data: {},
            success: 
            function( result )
            {
                mostrar_lienzo( result.registros );
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    console.error( err );
            }
        });

    //mostrar_lienzo( datos );

    if( e != null )
        e.preventDefault();
}

function obtener_menu( e )
{
    var idmenu = $(this).data("idmenu");
    var nodo = Object();

    if( !$.isWindow(this) )
    {
        nodo.idmenu = idmenu;
        nodo.texto = $(this).text();
    }

    $.ajax
        ({
            type: "GET",
            url: idmenu > 0 ? "ws/cliente/menus/"+idmenu : "ws/cliente/menus",
            dataType: "json",
            beforeSend: function(x) {
                if(x && x.overrideMimeType) {
                 x.overrideMimeType("application/j-son;charset=ISO-8859-1");
               }},
            data: {},
            success: 
            function( result )
            {
                bloquearUI( false );
                if( result.resultado )
                {
                    switch( result.tipo )
                    {
                        case 0 : // MENU NO CONFIGURADO
                        {
                            alert("Esta seccion esta vacia");
                        } break;
                        case 2 : // LISTA DE LIENZOS (CATEGORIA)
                        {
                            llenar_categoria( result.registros, nodo );
                        } break;
                        case 3 : // LIENZO
                        {
                            //console.log(result.registros);
                            mostrar_lienzo( result.registros );
                        } break;
                        default : // LISTA DE MENU
                        {
                            llenar_menu( result.registros, nodo, result.tipo );
                        } break;
                    }
                }
                else
                {

                }
            },
            error:
            function( err )
            {
                    bloquearUI( false );
                    console.error( err );
            }
        });

    if( e != null )
            e.preventDefault();
}

function quiz_siguiente_pregunta( e )
{
    //console.log($(this));
    var id      = $(this).attr('data-id');
    var index   = $("#myquiz").find(".question.active").index();
    var total   = $("#myquiz").find(".question").length;

    if( index+1 < total )
    {
        $("#myquiz")
            .find(".question")
            .removeClass("active");

        $("#myquiz")
            .find(".question")
            .eq( index + 1 )
            .addClass("active");

        if( index+2 == total )
        {
            $("#myquiz")
                .find(".next")
                .text("Finalizar Encuesta");
        }
        else
        {
            $("#myquiz")
                .find(".next")
                .text("Siguiente Pregunta");
        }
    }
    else
    {
        $("#myquiz").addClass("blurred");
        enviar_encuesta( id );
    }

    if( e != null )
            e.preventDefault();
}

function salirSistema( e )
{	
    e.preventDefault();
    $.ajax(
    {
            type: "GET",
            url: "ws/logout",
            dataType: "json",
            data: {},
            success:function(result)
            {
                    if(result.resultado)
                    {
                            setTimeout(function(){
                                    window.location.href = "/TigoEmbajador/login";	
                            },300);
                    }
                    else
                    {
                            alert(result.mensaje);
                    }
            },
            error: function(error)
            {
                    //console.log(error);
                    //console.error(error);
            }
    });
}

function seleccionar_menu( e )
{
    var opcion = $(this).data("opcion");

    switch( opcion )
    {
        case "encuestas":
        {
            $("#springboard").removeClass("active");
            $("#myquiz")
                .addClass("active")
                .find(".question")
                .removeClass("active")
                .eq(0)
                .addClass("active");
        } break;
        case "logout":
        {
            window.location = "index"
        } break;
        default:
        {
            $("#springboard").addClass("active");
            $("#myquiz").removeClass("active");
        }
    }

    $("body > nav").removeClass("active");

    if( e != null )
        e.preventDefault();
}

function videocontrolsTouchEvent(event) {
    if (event.touches.length > 0) {
        
    } else {
        if (device === 'celular') {
            canX = event.changedTouches[0].pageX - videocanvas.offsetLeft;
            canY = event.changedTouches[0].pageY - videocanvas.offsetTop;
            /*canX = event.targetTouches[0].pageX - can.offsetLeft;
            canY = event.targetTouches[0].pageY - can.offsetTop;
            */
            mouseX = canX;
            mouseY = canY;
            //alert(canX + ", " + canY);
            if (( mouseY >= (vcoordy - (vheight / 2))) && (mouseY <= ((vcoordy - (vheight / 2)) + vheight)) && (mouseX >= (vcoordx - (vwidth / 2))) && (mouseX <= ((vcoordx - (vwidth / 2)) + vwidth))) {
                if (videoElement.paused) {
                    videoElement.play();
                    alreadyPlayed = true;
                    var ctx = videocanvas.getContext("2d");
                    ctx.clearRect(0, 0, videocanvas.width, videocanvas.height);
                }
            }
            //ctx.drawImage(imageVideo, 0, 0, 508, 356, vcoordx - (vwidth / 2), vcoordy ÓÓ- (vheight / 2), vwidth, vheight);
        }
    }
}

function videocontrolsEvent(event) {
    if (timeWaited >= buttonWait) {
        timeWaited = 0;
    }
    var mouseX;
    var mouseY;

    var x;
    var y;
    if (event.pageX || event.pageY) {
        x = event.pageX;
        y = event.pageY;
    }
    else {
        x = event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        y = event.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    x -= videocanvas.offsetLeft;
    y -= videocanvas.offsetTop;

    var mouseXCel = x;
    var mouseYCel = y;
    //alert(mouseXCel + ", " + mouseYCel);
    
    var rect = videocanvas.getBoundingClientRect();
    //console.log('real');
    mouseX = event.clientX - rect.left;
    mouseY = event.clientY - rect.top;
    //alert(mouseY);
    
    //Hit Play
    videoElement = document.getElementsByTagName('video')[0];
    if (device === 'computadora') {
        if ( (mouseY >= 20) && (mouseY <= 51) && (mouseX >= 24) && (mouseX <= 52) ) {
            if (videoElement.paused) {
                videoElement.play();
                alreadyPlayed = true;
            }
        }

        //Hit Stop
        if ( (mouseY >= 20) && (mouseY <= 51) && (mouseX >= 86) && (mouseX <= 115) ) {
            videoElement.pause();
            videoElement.currentTime = 0;
        }

        //Hit Pause
        if ( (mouseY >= 20) && (mouseY <= 51) && (mouseX >= 55) && (mouseX <= 83) ) {
            if (videoElement.paused == false) {
                videoElement.pause();
            } else {
                videoElement.play();
            }
        }
    }
    
    //Hit play when mobile
    if (device === 'celular') {
        /*canX = event.targetTouches[0].pageX - can.offsetLeft;
        canY = event.targetTouches[0].pageY - can.offsetTop;
        alert(canX + ", " + canY);*/
        console.log(event.targetTouches);
        //alert(mouseY + ", " + mouseYCel + ", " + (vcoordy - (vheight / 2)));
        mouseX = mouseXCel;
        mouseY = mouseYCel;
        if (( mouseY >= (vcoordy - (vheight / 2))) && (mouseY <= ((vcoordy - (vheight / 2)) + vheight)) && (mouseX >= (vcoordx - (vwidth / 2))) && (mouseX <= ((vcoordx - (vwidth / 2)) + vwidth))) {
            if (videoElement.paused) {
                videoElement.play();
                alreadyPlayed = true;
                var ctx = videocanvas.getContext("2d");
                ctx.clearRect(0, 0, videocanvas.width, videocanvas.height);
            }
        }
        //ctx.drawImage(imageVideo, 0, 0, 508, 356, vcoordx - (vwidth / 2), vcoordy ÓÓ- (vheight / 2), vwidth, vheight);
    }
}

function gameLoop() {
    window.setTimeout(gameLoop, 20);
    drawScreen();	
}

function  drawScreen () {
    if (hasVideo) {
        videoElement = document.getElementsByTagName('video')[0];
        videocanvas = document.getElementById('videocanvas');
        if (videocanvas !== null && typeof videoElement !== 'undefined') {
            var ctx = videocanvas.getContext("2d");
            if (device === 'computadora') {
                var image = new Image();
                image.onload = function() {
                    ctx.clearRect(0, 0, videocanvas.width, videocanvas.height);
                    if (!videoElement.paused) {
                        //ctx.drawImage(imagePlay, 80, 0, 80, 65, 21, 20, 37, 30);
                        ctx.drawImage(image, 152, 162, 163, 148, 21, 20, 36, 36);
                        //ctx.drawImage(image, sx, sy, sWidth, sHeight, dx, dy, dWidth, dHeight);
                    } else {
                        //ctx.drawImage(imagePlay, 0, 0, 80, 65, 21, 20, 37, 30);
                        ctx.drawImage(image, 152, -4, 163, 148, 21, 20, 36, 36);
                    }

                    if (videoElement.paused) {
                        //ctx.drawImage(imagePlay, 240, 0, 80, 65, 51, 20, 37, 30);
                        //ctx.drawImage(image, 152, 2, 163, 148, 21, 20, 36, 36);
                        ctx.drawImage(image, -4, 162, 163, 148, 55, 20, 36, 36);
                    } else {
                        //ctx.drawImage(imagePlay, 160, 0, 80, 65, 51, 20, 37, 30);
                        //ctx.drawImage(image, 304, 0, 152, 182, 50, 20, 30, 36);
                        ctx.drawImage(image, -4, -4, 163, 148, 55, 20, 36, 36);
                    }

                    //ctx.drawImage(imagePlay, 320, 0, 80, 65, 81, 20, 37, 30);
                    //ctx.drawImage(image, 0, 0, 152, 182, 85, 20, 30, 36);
                    ctx.drawImage(image, 308, -4, 163, 148, 81, 20, 36, 36);
                };
                image.src = 'resources/images/botonesTigo.png';
            } else {
                if (videoElement.paused) {
                    var imageVideo = new Image();
                    imageVideo.onload = function() {
                        //console.log(vcoordx + "," + vcoordy + "-" + vwidth + "x" + vheight);
                        ctx.drawImage(imageVideo, 0, 0, 508, 356, vcoordx - (vwidth / 2), vcoordy - (vheight / 2), vwidth, vheight);
                    };
                    imageVideo.src = 'resources/images/bravo_video_play01.png';
                    /*ctx.fillStyle = 'rgba(225,225,225,0.5)';
                    ctx.fillRect(vcoordx - (vwidth / 2), vcoordy - (vheight / 2), vwidth, vheight);*/
                } else {
                    ctx.clearRect(0, 0, videocanvas.width, videocanvas.height);
                }
            }

            timeWaited++;
        }

    }
}