var kitchensink = { };
var canvas = new fabric.Canvas('canvas');
var idlienzo = null;
var duplicado = null;
var categoriaslienzo = Array();
$(document).on("ready", inicializar);

(function() {

	if (document.location.hash !== '#zoom') return;

	function renderVieportBorders() {
		var ctx = canvas.getContext();

		ctx.save();

		//ctx.fillStyle = 'rgba(0,0,0,0.1)';

		ctx.fillRect(
			canvas.viewportTransform[4],
			canvas.viewportTransform[5],
			canvas.getWidth() * canvas.getZoom(),
			canvas.getHeight() * canvas.getZoom());

		ctx.setLineDash([5, 5]);

		ctx.strokeRect(
			canvas.viewportTransform[4],
			canvas.viewportTransform[5],
			canvas.getWidth() * canvas.getZoom(),
			canvas.getHeight() * canvas.getZoom());

		ctx.restore();
	}

	$(canvas.getElement().parentNode).on('mousewheel', function(e) {

		var newZoom = canvas.getZoom() + e.deltaY / 300;
		canvas.zoomToPoint({ x: e.offsetX, y: e.offsetY }, newZoom);

		renderVieportBorders();

		return false;
	});

	var viewportLeft = 0,
	viewportTop = 0,
	mouseLeft,
	mouseTop,
	_drawSelection = canvas._drawSelection,
	isDown = false;

	canvas.on('mouse:down', function(options) {
		isDown = true;

		viewportLeft = canvas.viewportTransform[4];
		viewportTop = canvas.viewportTransform[5];

		mouseLeft = options.e.x;
		mouseTop = options.e.y;

		if (options.e.altKey) {
			_drawSelection = canvas._drawSelection;
			canvas._drawSelection = function(){ };
		}

		renderVieportBorders();
	});

	canvas.on('mouse:move', function(options) {
		if (options.e.altKey && isDown) {
			var currentMouseLeft = options.e.x;
			var currentMouseTop = options.e.y;

			var deltaLeft = currentMouseLeft - mouseLeft,
			deltaTop = currentMouseTop - mouseTop;

			canvas.viewportTransform[4] = viewportLeft + deltaLeft;
			canvas.viewportTransform[5] = viewportTop + deltaTop;

			canvas.renderAll();
			renderVieportBorders();
		}
	});

	canvas.on('mouse:up', function() {
		canvas._drawSelection = _drawSelection;
		isDown = false;
	});
})();

(function(){
	var mainScriptEl = document.getElementById('main');
	if (!mainScriptEl) return;
	var preEl = document.createElement('pre');
	var codeEl = document.createElement('code');
	codeEl.innerHTML = mainScriptEl.innerHTML;
	codeEl.className = 'language-javascript';
	preEl.appendChild(codeEl);
	document.getElementById('bd-wrapper').appendChild(preEl);
})();

(function() {
	fabric.util.addListener(fabric.window, 'load', function() {
		var canvas = this.__canvas || this.canvas,
		canvases = this.__canvases || this.canvases;

		canvas && canvas.calcOffset && canvas.calcOffset();

		if (canvases && canvases.length) {
			for (var i = 0, len = canvases.length; i < len; i++) {
				canvases[i].calcOffset();
			}
		}
	});
})();

function agregarMultimediaImage( e )
{
	var item = $("#modal-multimedia li.active");
	var id = $(item).data("id");
	var mime = $(item).data("mime");
	var source = $(item).data("url");
	var type = $(item).data("tipo");

	if( type == 1 )
	{
		fabric.Image.fromURL(source, function(image)
		{
			image.set(
			{
				left: 10,
				top: 10,
				angle: 0
			})
				.scale( 1 )
				.setCoords();

			canvas.add(image);

			bloquearUI( false );
			$("#modal-multimedia")
				.removeClass("active")
				.find("li")
				.removeClass("active");
		});
	}
	else
	{
		var video =
		$("<video/>")
			.append("<source src='" + source + "'></source>")
			.attr("id", "video-" + id)
			.attr("height", "360")
			.attr("width", "480")
			.attr("webkit-playsinline", "webkit-playsinline")
			.attr("controls", "controls");
                var videoType = supportedVideoFormat(video.get(0));
                if (videoType == "") {
                    alert("no video support");
                    return;
                }
		video.appendTo( "#canvas-htmlobjects" );
		var videoEl = document.getElementById( "video-" + id );
		var videoCanvas = new fabric.Image(videoEl, {
			left: 500,
			top: 500,
			angle: 0,
			originX: 'center',
			originY: 'center',
			uniqid: 'video-' + id
		});

		canvas.add( videoCanvas );
		videoCanvas.getElement().play();

                // TODO: Agregar controles al video

		fabric.util.requestAnimFrame(function render() {
			canvas.renderAll();
			fabric.util.requestAnimFrame(render);
		});

		bloquearUI( false );
		$("#modal-multimedia")
			.removeClass("active")
			.find("li")
			.removeClass("active");
	}
}

function bloquearUI( flag )
{
	if( flag )
	{
		$(".block-ui").show();
	}
	else
	{
		$(".block-ui").hide();
	}
}

function cambiarAltoLienzo( e )
{
	var alto = parseInt( $(this).val() );
	var ancho = canvas.getWidth();

	canvas.setDimensions( { width: ancho, height: alto } );
}

function cargarLienzo( id, duplicar )
{
	mostrarLoader( true );

	$.ajax
		({
			type: "GET",
			url: "http://192.168.88.208:18080/proelio-app/webapi/canvases/" + id,
			dataType: "json",
                        beforeSend: function(x) {
                            if(x && x.overrideMimeType) {
                             x.overrideMimeType("application/j-son;charset=ISO-8859-1");
                           }},
			data: {},
			success:
			function( result )
			{
				if( result.resultado )
				{
					var json = result.registros[0].config;

					idlienzo = id;

					canvas.loadFromJSON(json, function()
					{
						canvas.renderAll();

						// Search and destroy empty elements!
					    $.each( canvas.getObjects(), function()
					    {
					    	if( this.type == "image" && this.src == undefined )
					    	{
					    		canvas.remove( this );
					    	}
					    });
					    $("#canvas-htmlobjects").html( result.registros[0].htmlobjects );
					});

					$("#canvas-height")
						.val( result.registros[0].alto )
						.trigger( "keyup" );
                                        if (!duplicar) {
                                            categoriaslienzo = result.registros[0].categorias;
																						// console.log(result.registros[0]);
                                            $("#guardar-lienzo")
                                                    .find("input[name=nombre]")
                                                    .val( result.registros[0].nombre );
                                            $("#guardar-lienzo")
                                                    .find("input[name=descripcion]")
                                                    .val( result.registros[0].descripcion );
                                            $("#guardar-lienzo")
                                                    .find("input[name=keywords]")
                                                    .val( result.registros[0].keywords );
                                        } else {
                                            duplicado = duplicar;
                                        }

					mostrarLoader( false );
				}
			},
			error:
			function( err )
			{
                mostrarLoader( false );
				console.error( err );
			}
		});
}

function mostrarLoader( flag )
{
    if( flag )
    {
            $("#loader").addClass("active");
    }
    else
    {
            $("#loader").removeClass("active");
    }
}

function inicializar( e )
{
	var idlienzo = getUrlVars()['id'];
	// var idlienzo = getUrlParameter("id");

	// COLOR PICKER
	$('#picker-bg-sketch').colpick(
	{
		layout:'rgbhex',
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css('background-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor)
			{
				$(el).val( "#" + hex );
				var evento = jQuery.Event("select");
				$('#picker-bg-sketch').trigger( evento );
			}
		}
	}).keyup(function()
	{
		$(this).colpickSetColor(this.value);
	}).select(function()
	{
		$(this).colpickSetColor(this.value);
	}).change(function()
	{
		$(this).colpickSetColor(this.value);
	});

	$('#picker-obj-color').colpick(
	{
		layout:'rgbhex',
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css('background-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor)
			{
				$(el).val( "#" + hex );
				var evento = jQuery.Event("select");
				$('#picker-obj-color').trigger( evento );
			}
		}
	}).keyup(function()
	{
		$(this).colpickSetColor(this.value);
	}).select(function()
	{
		$(this).colpickSetColor(this.value);
	}).change(function()
	{
		$(this).colpickSetColor(this.value);
	});

        $('#picker-obj-color-stroke').colpick(
	{
		layout:'rgbhex',
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css('background-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor)
			{
				$(el).val( "#" + hex );
				var evento = jQuery.Event("select");
				$('#picker-obj-color-stroke').trigger( evento );
			}
		}
	}).keyup(function()
	{
		$(this).colpickSetColor(this.value);
	}).select(function()
	{
		$(this).colpickSetColor(this.value);
	}).change(function()
	{
		$(this).colpickSetColor(this.value);
	});

	$('#picker-bg-text').colpick(
	{
		layout:'rgbhex',
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css('background-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor)
			{
				$(el).val( "#" + hex );
				var evento = jQuery.Event("select");
				$('#picker-bg-text').trigger( evento );
			}
		}
	}).keyup(function()
	{
		$(this).colpickSetColor(this.value);
	}).select(function()
	{
		$(this).colpickSetColor(this.value);
	}).change(function()
	{
		$(this).colpickSetColor(this.value);
	});

	$('#picker-stroke-text').colpick(
	{
		layout:'rgbhex',
		submit:0,
		colorScheme:'dark',
		onChange:function(hsb,hex,rgb,el,bySetColor) {
			$(el).css('background-color','#'+hex);
			// Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
			if(!bySetColor)
			{
				$(el).val( "#" + hex );
				var evento = jQuery.Event("select");
				$('#picker-stroke-text').trigger( evento );
			}
		}
	}).keyup(function()
	{
		$(this).colpickSetColor(this.value);
	}).select(function()
	{
		$(this).colpickSetColor(this.value);
	}).change(function()
	{
		$(this).colpickSetColor(this.value);
	});
	// COLOR PICKER

	$("#btn-guardar-lienzo").on("click", mostrarGuardarLienzo);
	$("#guardar-lienzo").on("submit", guardarLienzo);
	$("#modal-guardar-lienzo").find(".close,.no").on("click", mostrarGuardarLienzo);
	$("#modal-multimedia").find(".close,.no").on("click", { tipo: 0 }, mostrarMultimedia);
	$("#modal-multimedia").find(".ok").on("click", agregarMultimediaImage);
	$("#canvas-height").on("keyup", cambiarAltoLienzo);
        $("#btn-ver-lienzo").on("click", mostrarPreview);

	// MOUSETRAP!
	Mousetrap.bind("backspace", function(e) { $("#remove-selected").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+1", function(e) { $("#drawing-mode").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+2", function(e) { $("#add-rect").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+3", function(e) { $("#add-circle").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+4", function(e) { $("#add-triangle").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+5", function(e) { $("#add-line").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+6", function(e) { $("#add-polygon").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+7", function(e) { $("#add-text").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+8", function(e) { $("#add-image").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+9", function(e) { $("#add-video").click(); e.preventDefault(); } );
	Mousetrap.bind("ctrl+backspace", function(e) { $("#sketch-clear").click(); e.preventDefault(); } );

	// SI ES EDICION DE LIENZO, CARGAMOS LA DATA
	if( idlienzo > 0 )
	{
            $("#btn-ver-lienzo").show();
						var duplicar = getUrlVars()['dup'];
            // var duplicar = getUrlParameter("dup");
            cargarLienzo( idlienzo, duplicar );
	}
}

function mostrarPreview( e )
{
    window.open("colaborador?id="+idlienzo);
    //window.open(canvas.toDataURL({format: 'png', quality: 1}));
}

function getUrlVars()
{
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for(var i = 0; i < hashes.length; i++)
    {
        hash = hashes[i].split('=');
        vars.push(hash[0]);
        vars[hash[0]] = hash[1];
    }
    return vars;
}

function getUrlParameter(sParam)
{
    var sPageURL = decodeURIComponent(window.location.hash.substring(1)),
        sURLVariables = sPageURL.split('&'),
        sParameterName,
        i;

    for (i = 0; i < sURLVariables.length; i++) {
        sParameterName = sURLVariables[i].split('=');

        if (sParameterName[0] === sParam) {
            return sParameterName[1] === undefined ? true : sParameterName[1];
        }
    }
}

function guardarLienzo( e )
{
    console.log(idlienzo);
    console.log(duplicado);
    console.log(idlienzo == null || duplicado != null);

    canvas.deactivateAllWithDispatch();
    var dataURL = canvas.toDataURL({format: 'png', quality: 1});
    var blob = dataURItoBlob(dataURL);
    var formData = new FormData(this);
    // validar que haya ingresado un nombre y al menos una categoria
    var datos = $(this).serialize();
    if (datos.split("&")[0].split("=")[1] === "") {
        alert("Debe ingresar un nombre.");
        return;
    }
    if ($("#sketch-categorias").val() === null) {
        alert("Debe seleccionar al menos una categoría.");
        return;
    }
    var videos = "<script>";
    /*datos = datos + "&categorias=" + $("#sketch-categorias").val().join();
    datos = datos + "&contenido=" + canvas.toSVG();
    datos = datos + "&alto=" + $("#canvas-height").val();
    datos = datos + "&ancho=" + "700";*/

    formData.append("categorias", $("#sketch-categorias option:selected").map(function () {
        return $(this).text();
    }).get().join());
    formData.append("contenido", canvas.toSVG());
    formData.append("alto", $("#canvas-height").val());
    formData.append("ancho", "700");
    formData.append("canvasImage", blob);

    // HTMLObjects contains <script> video too!
    $.each( canvas.getObjects(), function()
    {
    	if( this.uniqid != undefined )
    	{
    		var element = this.uniqid;
    		var height = this.height;
    		var left = this.left;
    		var scaleX = this.scaleX;
    		var scaleY = this.scaleY;
    		var top = this.top;
    		var width = this.width;

    		videos = videos
    				+ "var videoEl = document.getElementById( '" + element + "' );"
					+ "var videoCanvas = new fabric.Image(videoEl, {"
						+ "left: " + left + ","
						+ "top: " + top + ","
						+ "scaleX: " + scaleX + ","
						+ "scaleY: " + scaleY + ","
						+ "angle: 0,"
						+ "originX: 'center',"
						+ "originY: 'center',"
						+ "uniqid: '" + element + "'"
					+ "});"
    				+ "canvas.add( videoCanvas );"
					+ "videoCanvas.getElement().play();";

			$("#canvas-htmlobjects")
				.find("#" + this.uniqid)
				.attr("height", height)
				.attr("width", width)
				.css("transform", "scale(" + scaleX + "," + scaleY + ")");

    		//canvas.remove( this );
    	}
    });
    videos = videos
			+ "fabric.util.requestAnimFrame(function render() {"
				+ "canvas.renderAll();"
				+ "fabric.util.requestAnimFrame(render);"
			+ "});</script>";
    /*datos = datos + "&htmlobjects=" + $("#canvas-htmlobjects").html() + videos;
    datos = datos + "&config=" + JSON.stringify( canvas.toJSON() );*/
    formData.append("htmlobjects", $("#canvas-htmlobjects").html() + videos);
    formData.append("config", JSON.stringify( canvas.toJSON() ));

    mostrarLoader( true );
	$.ajax
		({
			type: "POST",
			url: (idlienzo == null || duplicado)? "http://192.168.88.208:18080/proelio-app/webapi/canvases" : "http://192.168.88.208:18080/proelio-app/webapi/canvases/" + idlienzo,
			dataType: "json",
			data: formData,
                        mimeType: "multipart/form-data",
                        contentType:    false,
                        processData:    false,
			success:
			function( result )
			{
                            if( result.resultado )
                            {
                                //hideLoader();

                                alert(result.mensaje);
                                mostrarLoader( false );
                                mostrarGuardarLienzo( null );
                                idlienzo = result.id;
                                $("#btn-ver-lienzo").show();

                                nGen('success', 'fa fa-check-circle', 'Exito..!', result.mensaje, 'topRight');
                                $('#modal-crear').modal('hide');
                                setTimeout( function(){ ratPack.refresh(); } , 300 );
                            }
                            else
                            {
                                alert(result.mensaje);
                                nGen('warning', 'fa fa-times-circle', 'Espera..!', result.mensaje , 'topRight');
                                mostrarLoader( false );
                            }
			},
			error:
			function( err )
			{
                mostrarLoader( false );
				console.error( err );
			}
		});
	e.preventDefault();
}

function mostrarGuardarLienzo( e )
{
	bloquearUI( true );

	if( $("#modal-guardar-lienzo").hasClass("active") )
	{
		bloquearUI( false );
		$("#modal-guardar-lienzo").removeClass("active");
	}
	else
	{
		$.ajax
			({
				type: "GET",
				url: "http://192.168.88.208:18080/proelio-app/webapi/canvases/tags",
				dataType: "json",
                                beforeSend: function(x) {
                                    if(x && x.overrideMimeType) {
                                     x.overrideMimeType("application/j-son;charset=ISO-8859-1");
                                   }},
				data: {},
				success:
				function( result )
				{
					bloquearUI( false );
					$("#modal-guardar-lienzo").addClass("active");

					if( result.resultado )
					{
						var registros = result.registros;
						$("#sketch-categorias").empty();

						for( x in registros )
						{
							var option =
							$("<option/>")
								.val( registros[x].id )
								.text( registros[x].description )
								.appendTo("#sketch-categorias");

								// console.log(registros[x]);
								// console.log(categoriaslienzo);
							for( y in categoriaslienzo )
							{
								if( registros[x].name == categoriaslienzo[y].nombre )
									$(option).attr("selected", "selected");
							}
						}

						$("#sketch-categorias").chosen();
					}
				},
				error:
				function( err )
				{
					console.error( err );
				}
			});
	}

        if( e != null )
	e.preventDefault();
}

function mostrarMultimedia( e )
{
	var tipomultimedia = e.data.tipo;

	bloquearUI( true );

	if( $("#modal-multimedia").hasClass("active") )
	{
		bloquearUI( false );
		$("#modal-multimedia").removeClass("active");
	}
	else
	{
		$.ajax
			({
				type: "GET",
				url: "ws/cliente/multimedias",
				dataType: "json",
                                beforeSend: function(x) {
                                    if(x && x.overrideMimeType) {
                                     x.overrideMimeType("application/j-son;charset=ISO-8859-1");
                                   }},
				data: { tipo : tipomultimedia },
				success:
				function( result )
				{
					bloquearUI( false );
					$("#modal-multimedia").addClass("active");

					if( result.resultado )
					{
						var registros = result.registros;
						$("#modal-multimedia ul.multimedia").empty();

						for( x in registros )
						{
                                                        var imagen = new Image();
							if( registros[x].tipo == 1 )
							{
								imagen.src = registros[x].url;
							} else {
                                                            imagen.src = registros[x].url.substring(0, registros[x].url.indexOf(".")) + ".jpg";

                                                            /*var video = document.createElement('video');
                                                            video.src = registros[x].url;
                                                            video.addEventListener('loadedmetadata', function() {
                                                                video.currentTime = 1;
                                                            });

                                                            video.addEventListener('seeked', function() {
                                                                var w = video.videoWidth;
                                                                var h = video.videoHeight;
                                                                var canvas = document.createElement('canvas');
                                                                canvas.width = w;
                                                                canvas.height = h;
                                                                canvas.getContext('2d').drawImage(video, 0, 0, w, h);
                                                                var data = canvas.toDataURL("image/jpg");
                                                                imagen.src = data;
                                                            });*/
                                                        }

                                                        var captiondiv = $("<div class=\"caption\">").append("<h3>" + registros[x].nombre + "</h3>");
                                                        var maindiv = $("<div class=\"thumbnail\"/>")
                                                            .append(captiondiv)
                                                            .append(imagen)
                                                        $("<li/>")
                                                                .append(maindiv)
								.attr( "data-id", registros[x].id )
								.attr( "data-url", registros[x].url )
								.attr( "data-mime", registros[x].mime )
								.attr( "data-tipo", registros[x].tipo )
								.on("click", seleccionarMultimedia)
								.appendTo("#modal-multimedia ul.multimedia");
						}
					}
				},
				error:
				function( err )
				{
					console.error( err );
				}
			});
	}

	if( e != null )
		e.preventDefault();
}

function seleccionarMultimedia( e )
{
	if( $(this).hasClass("active") )
	{
		$(this)
			.parent()
			.find("li")
			.removeClass("active");
	}
	else
	{
		$(this)
			.parent()
			.find("li")
			.removeClass("active");
		$(this).addClass("active");
	}
}

function video()
{
	var video1El = document.getElementById('video1');
	var video1 = new fabric.Image(video1El, {
		left: 350,
		top: 300,
		angle: 0,
		originX: 'center',
		originY: 'center'
	});
	canvas.add(video1);
	video1.getElement().play();

	fabric.util.requestAnimFrame(function render() {
		canvas.renderAll();
		fabric.util.requestAnimFrame(render);
	});
}

function supportedVideoFormat(video) {
    var returnExtension = "";
    if (video.canPlayType("video/webm") == "probably" || video.canPlayType("video/webm") == "maybe") {
        returnExtension = "webm";
    } else if(video.canPlayType("video/mp4") == "probably" || video.canPlayType("video/mp4") == "maybe") {
        returnExtension = "mp4";
    } else if(video.canPlayType("video/ogg") == "probably" || video.canPlayType("video/ogg") == "maybe") {
        returnExtension = "ogg";
    }

    return returnExtension;
}

function nGen(type, icono , titulo ,text, layout) {
    var n = noty({
        text: '<div class="activity-item"> <i class="'+icono+'" + "text-alert"></i> <div class="activity"> <span>'+ titulo +'</span> <span>'+ text +'</span> </div> </div>',
        type: type,
        dismissQueue: true,
        layout: layout,
        timeout     : 3000,
        closeWith: ['click'],
        theme: 'MatMixNoty',
        maxVisible: 10,
        animation: {
            open: 'noty_animated bounceInRight',
            close: 'noty_animated bounceOutRight',
            easing: 'swing',
            speed: 500
        }
    });

}

function dataURItoBlob(dataURI) {
    // convert base64/URLEncoded data component to raw binary data held in a string
    var byteString;
    if (dataURI.split(',')[0].indexOf('base64') >= 0)
        byteString = atob(dataURI.split(',')[1]);
    else
        byteString = unescape(dataURI.split(',')[1]);

    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to a typed array
    var ia = new Uint8Array(byteString.length);
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }

    return new Blob([ia], {type:mimeString});
}
