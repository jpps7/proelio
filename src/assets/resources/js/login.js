
$( "#formLogin" ).on( "submit" , doLogin );

function showLoader() {
    jQuery("#loader").removeClass("hide");
}

function hideLoader() {
    jQuery("#loader").addClass("hide");
}

function getMobileOperatingSystem() {
  var userAgent = navigator.userAgent || navigator.vendor || window.opera;

  if( userAgent.match( /iPad/i ) || userAgent.match( /iPhone/i ) || userAgent.match( /iPod/i ) )
  {
    return '3';

  }
  else if( userAgent.match( /Android/i ) )
  {

    return '2';
  }
  else
  {
    return '1';
  }
}

function doLogin( e ) 
{
    e.preventDefault();
    e.stopPropagation();
    
    showLoader();
    jQuery.ajax({
        type:       "POST",
        url:        "ws/login",
        dataType:   "json",
        data:       jQuery(this).serialize()+"&sistema="+getMobileOperatingSystem(),
        success: function( resultado ) {
            if( resultado.resultado )
            {      
                nGen('success', 'fa fa-times-circle', 'Espera..!', resultado.mensaje , 'topRight');    
                setTimeout("window.location.href = 'colaborador'", 700);
            }
            else
            {
                hideLoader();
                nGen('warning', 'fa fa-times-circle', 'Espera..!', resultado.mensaje , 'topRight');
            }
        },
        error: function( error ) {
            hideLoader();
            console.log(error);
            alert("Ocurrio un error, vuelve a intentar");
        }
    });
    
}

function nGen(type, icono , titulo ,text, layout) {
    var n = noty({
        text: '<div class="activity-item"> <i class="'+icono+'" + "text-alert"></i> <div class="activity"> <span>'+ titulo +'</span> <span>'+ text +'</span> </div> </div>',
        type: type,
        dismissQueue: true,
        timeout     : 3000,
        layout: layout,
        closeWith: ['click'],
        theme: 'MatMixNoty',
        maxVisible: 10,
        animation: {
            open: 'noty_animated bounceInRight',
            close: 'noty_animated bounceOutRight',
            easing: 'swing',
            speed: 500
        }
    });

}
