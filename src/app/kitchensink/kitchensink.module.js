/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.kitchensink', ['Proelio.kitchensink.wizard'])
      .directive('bindValueTo', bindValueTo)
      .directive('objectButtonsEnabled', objectButtonsEnabled);

  /** @ngInject */
  function bindValueTo() {
    return {
      restrict: 'AE',

      link: function ($scope, $element, $attrs) {

        var prop = capitalize($attrs.bindValueTo),
            getter = 'get' + prop,
            setter = 'set' + prop;

        $element.on('select change keyup', function() {
          $scope[setter] && $scope[setter](this.value);
        });

        $scope.$watch($scope[getter], function(newVal) {
          if ($element[0].type === 'radio') {
            var radioGroup = document.getElementsByName($element[0].name);
            for (var i = 0, len = radioGroup.length; i < len; i++) {
              radioGroup[i].checked = radioGroup[i].value === newVal;
            }
          }
          else {
            $element.val(newVal);

            var evento = jQuery.Event('change');
            jQuery( $element ).trigger( evento );
          }
        });
      }
    };
  }

  function objectButtonsEnabled() {
    return {
      restrict: 'AE',

      link: function ($scope, $element, $attrs) {
        $scope.$watch($attrs.objectButtonsEnabled, function(newVal) {

          $($element).find('.btn-object-action')
            .prop('disabled', !newVal);
        });
      }
    };
  }

})();
