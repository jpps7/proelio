/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.kitchensink.wizard', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    // $stateProvider
    //   .state('wizard', {
    //     url: '/dashboard/wizard',
    //     templateUrl: 'app/kitchensink/wizard/wizard.html',
    //     controller: 'WizardCtrl',
    //     title: 'Wizard',
    //     data: {
    //       authenticate: true,
    //       wizard: true,
    //       classes: {
    //         html: 'wizard-layout',
    //         uiView: 'wizard-main'
    //       }
    //     }
    //   });
  }

})();
