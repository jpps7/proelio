/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.kitchensink.wizard')
      .controller('WizardCtrl', WizardCtrl);

  /** @ngInject */
  function WizardCtrl($scope) {

  }
})();
