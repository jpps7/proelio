/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.theme.components')
    .controller('UserPageTopCtrl', UserPageTopCtrl);

  /** @ngInject */
  function UserPageTopCtrl($scope, $state, authService) {
    $scope.doLogout = function() {
      authService.logout();
    }

    $scope.$on('logout:successful', function() {
      $state.transitionTo('login');
    });

  }
})();
