/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.theme.components')
      .directive('userPageTop', userPageTop);

  /** @ngInject */
  function userPageTop() {
    return {
      restrict: 'AE',
      templateUrl: 'app/theme/components/pageTop/userPageTop.html',
      controller: 'UserPageTopCtrl'
    };
  }

})();
