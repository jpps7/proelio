/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .config(config);

  /** @ngInject */
  function config(baConfigProvider, colorHelper, RestangularProvider, $stateProvider, $urlRouterProvider, baSidebarServiceProvider, API) {
    RestangularProvider.setBaseUrl(API.url);
    RestangularProvider.setDefaultHeaders({'Content-Type': 'application/json'});
    RestangularProvider.setRequestInterceptor(function(elem, operation) {
      if (operation === "remove") {
        return undefined;
      }
      return elem;
    });

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/pages/login/login.html',
        controller: 'LoginCtrl',
        title: 'Login',
        data: {
          classes: {
            html: 'login-layout',
            uiView: 'login-main'
          }
        }
      });

    $stateProvider
      .state('recovery', {
        url: '/password/reset',
        templateUrl: 'app/pages/recovery/recovery.html',
        controller: 'RecoveryCtrl',
        title: 'Password Reset',
        data: {
          classes: {
            html: 'recovery-layout',
            uiView: 'recovery-main'
          }
        }
      });

    $urlRouterProvider.otherwise('/login');
  }
})();
