/*
@author Juan Pablo Pinto Santana
@company Jumay
created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.theme')
      .factory('handleResponse', handleResponse);

  /** @ngInject */
  function handleResponse(toastr, API) {
    return {
      success: function(response) {
        var data;
        var message;
        if (response[API.response.codigo]) {
          data = response[API.response.data] ? response[API.response.data] : null;
          message = response[API.response.message] ? response[API.response.message] : '';
          switch(response[API.response.codigo]) {
            case 200:
              toastr.success(message, 'Éxito');
              break;
            default:
              toastr.warning(message, 'Código: ' + response[API.response.codigo]);
          }
          return data;
        } else {
          toastr.warning('No es posible obtener el código');
        }
      },
      error: function(response) {
        toastr.error(response.statusText, 'Código: ' + response.status);
      }
    };
  }
})();
