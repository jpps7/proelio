/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('handleStates', handleStates);

  /** @ngInject */
  function handleStates(runtimeStatesProvider, localStorage) {

    var TOKEN_KEY = 'States';

    function useStates(states) {
      runtimeStatesProvider.addStates(states);
    }

    return {
      loadStates: function() {
        var states = localStorage.getObject(TOKEN_KEY, '{}');
        if(states) {
          useStates(states);
        }
      },
      storeStates: function(states) {
        localStorage.storeObject(TOKEN_KEY, states);
        useStates(states);
      },
      destroyStates: function() {
        localStorage.remove(TOKEN_KEY);
        runtimeStatesProvider.reset();
      }
    }
  }

})();
