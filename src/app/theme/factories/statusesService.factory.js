/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('statusesService', statusesService);

  /** @ngInject */
  function statusesService($rootScope, API, proelioResource, handleResponse) {
    var getFunctions = {
      success: function(response) {
        var statuses = handleResponse.success(response);
        $rootScope.$broadcast('statuses:loaded:success', statuses);
      },
      error: function(response) {
        handleResponse.error(response);
      }
    };

    var getFunctionsAlternate = {
      success: function(response) {
        var statuses = handleResponse.success(response);
        $rootScope.$broadcast('statuses_user:loaded:success', statuses);
      },
      error: function(response) {
        handleResponse.error(response);
      }
    };

    function load() {
      proelioResource.get(API.endpoints.statuses, _, getFunctions.success, getFunctions.error, _);
    }

    function loadAlternate() {
      proelioResource.get(API.endpoints.statuses_user, _, getFunctionsAlternate.success, getFunctionsAlternate.error, _);
    }

    return {
      get: function(option) {
        if (option === 'users') {
          loadAlternate()
        } else {
          load();
        }
      }
    }
  }
})();
