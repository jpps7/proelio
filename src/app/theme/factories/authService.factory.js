/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('authService', authService);

  /** @ngInject */
  function authService($http, $rootScope, API, proelioResource, localStorage, handleResponse, runtimeStates, baSidebarService) {
    var TOKEN_KEY = 'User';
    var TOKEN_KEY_STATES = 'States';
    var TOKEN_KEY_MENU = 'Menu';
    var isAuthenticated = false;
    var username = '';
    var authToken = null;

    function loadCredentials() {
      var credentials = localStorage.getObject(TOKEN_KEY, '{}');
      if(credentials.username !== undefined) {
        useCredentials(credentials);
      }
    }

    function loadStates() {
      var states = localStorage.getObject(TOKEN_KEY_STATES, null);
      if(states) {
        useStates(states);
      }
    }

    function loadMenuItems() {
      var items = localStorage.getObject(TOKEN_KEY_MENU, null);
      if(items) {
        useMenuItems(items);
      }
    }

    function storeStates(states) {
      localStorage.storeObject(TOKEN_KEY_STATES, states);
      useStates(states);
    }

    function storeMenuItems(items) {
      localStorage.storeObject(TOKEN_KEY_MENU, items);
      useMenuItems(items);
    }

    function storeCredentials(credentials) {
      localStorage.storeObject(TOKEN_KEY, credentials);
      useCredentials(credentials);
    }

    function useStates(states) {
      runtimeStates.addStates(states);
    }

    function useMenuItems(items) {
      runtimeStates.addMenuItems(items);
    }

    function useCredentials(credentials) {
      isAuthenticated = true;
      username = credentials.username;
      authToken = credentials.token;
      $http.defaults.headers.common['Token'] = authToken;
    }

    function destroyCredentials() {
      authToken = null;
      username = '';
      isAuthenticated = false;
      delete $http.defaults.headers.common['Token'];
      localStorage.remove(TOKEN_KEY);
    }

    function destroyStates() {
      localStorage.remove(TOKEN_KEY_STATES);
    }

    function destroyMenuItems() {
      localStorage.remove(TOKEN_KEY_MENU);
    }

    var loginFunctions = {
      success: function(response) {
        var data = handleResponse.success(response)[0];
        if(!!data['username'] && !!data['token']) {
          storeCredentials({username: data['username'], token: data['token']});
          if(data['states']) {
            storeStates(data['states']);
          }
          if(data['menu']) {
            storeMenuItems(data['menu']);
          }
          $rootScope.$broadcast('login:successful');
        } else {
          isAuthenticated = false;
          $rootScope.$broadcast('login:unsuccessful');
        }
      },
      error: function(response) {
        isAuthenticated = false;
        handleResponse.error(response);
        $rootScope.$broadcast('login:unsuccessful');
      }
    };

    var logoutFunctions = {
      success: function(response) {
        handleResponse.success(response);
        destroyCredentials();
        destroyMenuItems();
        destroyStates();
        runtimeStates.reset();
        $rootScope.$broadcast('logout:successful');
      },
      error: function() {
        handleResponse.error(response);
        destroyCredentials();
        destroyMenuItems();
        destroyStates();
        runtimeStates.reset();
        $rootScope.$broadcast('logout:unsuccessful');
      }
    };

    loadCredentials();
    loadStates();
    loadMenuItems();

    return {
      login: function(credentials) {
        proelioResource.post(API.endpoints.users + '/' + API.endpoints.login, credentials, loginFunctions.success, loginFunctions.error, _);
      },
      logout: function() {
        proelioResource.post(API.endpoints.users + '/' + API.endpoints.logout, {username: username}, logoutFunctions.success, logoutFunctions.error, _);
      },
      isAuthenticated: function() {
        return isAuthenticated;
      },
      getUsername: function() {
        return username;
      }
    }
  }
})();
