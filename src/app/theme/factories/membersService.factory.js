/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('membersService', membersService);

  /** @ngInject */
  function membersService($rootScope, API, proelioResource, handleResponse) {

    var groupId;

    var addFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load(groupId);
        $rootScope.$broadcast('member:added:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('member:added:failure');
      }
    };

    var deleteFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load(groupId);
        $rootScope.$broadcast('member:deleted:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('member:deleted:failure');
      }
    };

    var getFunctions = {
      success: function(response) {
        var members = handleResponse.success(response);
        var send = members ? {message: 'members:loaded:success', data: members} : {message: 'members:loaded:failure'};
        $rootScope.$broadcast(send.message, send.data);
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('members:loaded:failure');
      }
    };

    function setGroupId(id) {
      console.log(id);
      groupId = id;
    }

    function load(id) {
      proelioResource.get(API.endpoints.groups + '/' + id + '/' + API.endpoints.members, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function(id) {
        $rootScope.$broadcast('members:loading');
        setGroupId(id);
        load(id);
      },
      add: function(member) {
        console.log(groupId);
        proelioResource.post(API.endpoints.groups + '/' + groupId + '/' + API.endpoints.members, member, addFunctions.success, addFunctions.error, _);
      },
      delete: function(member) {
        console.log(groupId);
        proelioResource.delete(API.endpoints.groups + '/' + groupId + '/' + API.endpoints.members, member.id, deleteFunctions.success, deleteFunctions.error, _);
      }
    }
  }
})();
