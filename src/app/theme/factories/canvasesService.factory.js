/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('canvasesService', canvasesService);

  /** @ngInject */
  function canvasesService($rootScope, API, proelioResource, handleResponse) {
    var deleteFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load();
        $rootScope.$broadcast('canvas:deleted:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('canvas:deleted:failure');
      }
    };

    var getFunctions = {
      success: function(response) {
        var canvases = handleResponse.success(response);
        var send = canvases ? {message: 'canvases:loaded:success', data: canvases} : {message: 'canvases:loaded:failure'};
        $rootScope.$broadcast(send.message, send.data);
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('canvases:loaded:failure');
      }
    };

    function load() {
      proelioResource.get(API.endpoints.canvases, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function() {
        $rootScope.$broadcast('canvases:loading');
        load();
      },
      delete: function(canvas) {
        proelioResource.delete(API.endpoints.canvases, canvas.id, deleteFunctions.success, deleteFunctions.error, _);
      }
    }
  }
})();
