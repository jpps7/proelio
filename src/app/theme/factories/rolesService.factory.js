/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('rolesService', rolesService);

  /** @ngInject */
  function rolesService($rootScope, API, proelioResource, handleResponse) {
    var getFunctions = {
      success: function(response) {
        var roles = handleResponse.success(response);
        $rootScope.$broadcast('roles:loaded:success', roles);
      },
      error: function(response) {
        handleResponse.error(response);
      }
    };

    function load() {
      proelioResource.get(API.endpoints.roles, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function() {
        load();
      }
    }
  }
})();
