/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('usersService', usersService);

  /** @ngInject */
  function usersService($rootScope, API, proelioResource, handleResponse) {
    var editFunctions = {
      success: function(response) {
        handleResponse.success(response);
        $rootScope.$broadcast('user:edited:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('user:edited:failure');
      }
    };

    var addFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load();
        $rootScope.$broadcast('user:added:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('user:added:failure');
      }
    };

    var deleteFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load();
        $rootScope.$broadcast('user:deleted:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('user:deleted:failure');
      }
    };

    var getFunctions = {
      success: function(response) {
        var users = handleResponse.success(response);
        var send = users ? {message: 'users:loaded:success', data: users} : {message: 'users:loaded:failure'};
        $rootScope.$broadcast(send.message, send.data);
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('users:loaded:failure');
      }
    };

    function load() {
      proelioResource.get(API.endpoints.users, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function() {
        $rootScope.$broadcast('users:loading');
        load();
      },
      edit: function(user) {
        proelioResource.put(API.endpoints.users, user.id, user, editFunctions.success, editFunctions.error, _);
      },
      add: function(user) {
        user.password = '12345';
        proelioResource.post(API.endpoints.users, user, addFunctions.success, addFunctions.error, _);
      },
      delete: function(user) {
        proelioResource.delete(API.endpoints.users, user.id, deleteFunctions.success, deleteFunctions.error, _);
      }
    }
  }
})();
