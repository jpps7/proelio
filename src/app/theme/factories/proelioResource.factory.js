/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.theme')
      .factory('proelioResource', proelioResource);

  /** @ngInject */
  function proelioResource(Restangular) {
    return {
      // post: function (route, data) {
      //   return Restangular.all(route).post(data);
      // },
      // get: function (route) {
      //   return Restangular.one(route).get();
      // },
      // put: function (route, id, data) {
      //   return Restangular.one(route, id).put(data);
      // },
      // delete: function (route, id) {
      //   return Restangular.one(route, id).remove();
      // },
      post: function(route, data, success, error, final) {
        var checkedData = angular.isObject(data) ? data : {};
        Restangular.all(route).post(checkedData).then(
          function(response) {
            if(success && angular.isFunction(success)) {
              success(response);
            }
          },
          function(response) {
            if(error && angular.isFunction(error)) {
              error(response);
            }
          }
        ).finally(function() {
          if(final && angular.isFunction(final)) {
            final();
          }
        });
      },
      get: function(route, data, success, error, final) {
        var checkedData = angular.isObject(data) ? data : {};
        Restangular.one(route).get(checkedData).then(
          function(response) {
            if(success && angular.isFunction(success)) {
              success(response);
            }
          },
          function(response) {
            if(error && angular.isFunction(error)) {
              error(response);
            }
          }
        ).finally(function() {
          if(final && angular.isFunction(final)) {
            final();
          }
        });
      },
      put: function (route, id, data, success, error, final) {
        var checkedData = angular.isObject(data) ? data : {};
        Restangular.one(route, id).customPUT(checkedData).then(
          function(response) {
            if(success && angular.isFunction(success)) {
              success(response);
            }
          },
          function(response) {
            if(error && angular.isFunction(error)) {
              error(response);
            }
          }
        ).finally(function() {
          if(final && angular.isFunction(final)) {
            final();
          }
        });
      },
      delete: function (route, id, success, error, final) {
        Restangular.one(route, id).remove().then(
          function(response) {
            if(success && angular.isFunction(success)) {
              success(response);
            }
          },
          function(response) {
            if(error && angular.isFunction(error)) {
              error(response);
            }
          }
        ).finally(function() {
          if(final && angular.isFunction(final)) {
            final();
          }
        });
      }
    };
  }
})();
