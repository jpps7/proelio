/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('homeService', homeService);

  /** @ngInject */
  function homeService($rootScope, API, proelioResource, handleResponse) {
    var getFunctions = {
      success: function(response) {
        var canvases = handleResponse.success(response);
        $rootScope.$broadcast('home:loaded:success', canvases);
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('home:loaded:failure');
      }
    };

    function load(id) {
      proelioResource.get(API.endpoints.users + '/' + id + '/' + API.endpoints.home, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function(id) {
        $rootScope.$broadcast('home:loading');
        load(id);
      }
    }
  }
})();
