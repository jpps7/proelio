/**
 * @author a.demeshko
 * created on 12/21/15
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('tagsService', tagsService);

  /** @ngInject */
  function tagsService($rootScope, API, proelioResource, handleResponse) {
    var editFunctions = {
      success: function(response) {
        handleResponse.success(response);
        $rootScope.$broadcast('tag:edited:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('tag:edited:failure');
      }
    };

    var addFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load();
        $rootScope.$broadcast('tag:added:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('tag:added:failure');
      }
    };

    var deleteFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load();
        $rootScope.$broadcast('tag:deleted:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('tag:deleted:failure');
      }
    };

    var getFunctions = {
      success: function(response) {
        var tags = handleResponse.success(response);
        var send = tags ? {message: 'tags:loaded:success', data: tags} : {message: 'tags:loaded:failure'};
        $rootScope.$broadcast(send.message, send.data);
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('tags:loaded:failure');
      }
    };

    function load() {
      proelioResource.get(API.endpoints.tags, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function() {
        $rootScope.$broadcast('tags:loading');
        load();
      },
      edit: function(tag) {
        proelioResource.put(API.endpoints.tags, tag.name, tag, editFunctions.success, editFunctions.error, _);
      },
      add: function(tag) {
        proelioResource.post(API.endpoints.tags, tag, addFunctions.success, addFunctions.error, _);
      },
      delete: function(tag) {
        proelioResource.delete(API.endpoints.tags, tag.name, deleteFunctions.success, deleteFunctions.error, _);
      }
    }
  }
})();
