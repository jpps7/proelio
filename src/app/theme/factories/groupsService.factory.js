/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('groupsService', groupsService);

  /** @ngInject */
  function groupsService($rootScope, API, proelioResource, handleResponse) {
    var editFunctions = {
      success: function(response) {
        handleResponse.success(response);
        $rootScope.$broadcast('group:edited:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('group:edited:failure');
      }
    };

    var addFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load();
        $rootScope.$broadcast('group:added:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('group:added:failure');
      }
    };

    var deleteFunctions = {
      success: function(response) {
        handleResponse.success(response);
        load();
        $rootScope.$broadcast('group:deleted:success');
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('group:deleted:failure');
      }
    };

    var getFunctions = {
      success: function(response) {
        var groups = handleResponse.success(response);
        var send = groups ? {message: 'groups:loaded:success', data: groups} : {message: 'groups:loaded:failure'};
        $rootScope.$broadcast(send.message, send.data);
      },
      error: function(response) {
        handleResponse.error(response);
        $rootScope.$broadcast('groups:loaded:failure');
      }
    };

    function load() {
      proelioResource.get(API.endpoints.groups, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function() {
        $rootScope.$broadcast('groups:loading');
        load();
      },
      edit: function(group) {
        proelioResource.put(API.endpoints.groups, group.id, group, editFunctions.success, editFunctions.error, _);
      },
      add: function(group) {
        proelioResource.post(API.endpoints.groups, group, addFunctions.success, addFunctions.error, _);
      },
      delete: function(group) {
        proelioResource.delete(API.endpoints.groups, group.id, deleteFunctions.success, deleteFunctions.error, _);
      }
    }
  }
})();
