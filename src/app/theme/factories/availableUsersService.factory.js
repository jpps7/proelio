/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .factory('availableUsersService', availableUsersService);

  /** @ngInject */
  function availableUsersService($rootScope, API, proelioResource, handleResponse) {
    var getFunctions = {
      success: function(response) {
        var users = handleResponse.success(response);
        $rootScope.$broadcast('available_users:loaded:success', users);
      },
      error: function(response) {
        handleResponse.error(response);
      }
    };

    function load(id) {
      console.log(id);
      proelioResource.get(API.endpoints.groups + '/' + id + '/' + API.endpoints.available_users, _, getFunctions.success, getFunctions.error, _);
    }

    return {
      get: function(id) {
        load(id);
      }
    }
  }
})();
