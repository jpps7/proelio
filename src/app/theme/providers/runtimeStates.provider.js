/**
 * Created by k.danovsky on 13.05.2016.
 */

(function () {
  'use strict';

  angular.module('Proelio.theme')
    .provider('runtimeStates', runtimeStatesProvider);

  /** @ngInject */
  function runtimeStatesProvider($stateProvider, $urlRouterProvider, baSidebarServiceProvider) {
    function init() {
      $stateProvider.init();
      $urlRouterProvider.init();
      baSidebarServiceProvider.init();

      $stateProvider
        .state('login', {
          url: '/login',
          templateUrl: 'app/pages/login/login.html',
          controller: 'LoginCtrl',
          title: 'Login',
          data: {
            classes: {
              html: 'login-layout',
              uiView: 'login-main'
            }
          }
        });

      $stateProvider
        .state('recovery', {
          url: '/password/reset',
          templateUrl: 'app/pages/recovery/recovery.html',
          controller: 'RecoveryCtrl',
          title: 'Password Reset',
          data: {
            classes: {
              html: 'recovery-layout',
              uiView: 'recovery-main'
            }
          }
        });

      $urlRouterProvider.otherwise('/login');
    }

    this.$get = function() {
      return {
        addState: function(name, data) {
          $stateProvider.state(name, data);
        },
        addStates: function(states) {
          console.log(states);
          states.forEach(function(state, index) {
            $stateProvider.state(state.name, state.data);
          });
        },
        addMenuItems: function(items) {
          items.forEach(function(item, index) {
            baSidebarServiceProvider.addStaticItem(item);
          });
        },
        setDefaultRoute: function(route) {
          $urlRouterProvider.otherwise(route);
        },
        reset: function() {
          init();
        }
      }
    }
  }
})();
