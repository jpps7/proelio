/**
 * @author v.lugovksy
 * created on 15.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.theme')
    .run(themeRun);

  /** @ngInject */
  function themeRun($timeout, $rootScope, $state, layoutPaths, preloader, $q, baSidebarService, themeLayoutSettings, authService) {
    var whatToWait = [
      preloader.loadAmCharts(),
      $timeout(3000)
    ];

    var theme = themeLayoutSettings;
    if (theme.blur) {
      if (theme.mobile) {
        whatToWait.unshift(preloader.loadImg(layoutPaths.images.root + 'blur-bg-mobile.jpg'));
      } else {
        whatToWait.unshift(preloader.loadImg(layoutPaths.images.root + 'blur-bg.jpg'));
        whatToWait.unshift(preloader.loadImg(layoutPaths.images.root + 'blur-bg-blurred.jpg'));
      }
    }

    $q.all(whatToWait).then(function () {
      $rootScope.$pageFinishedLoading = true;
    });

    $timeout(function () {
      if (!$rootScope.$pageFinishedLoading) {
        $rootScope.$pageFinishedLoading = true;
      }
    }, 7000);

    $rootScope.$baSidebarService = baSidebarService;

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
        if(toState.title) {
            $rootScope.pageTitle = toState.title + ' | Proelio';
        }
        if(toState.name === 'login' && authService.isAuthenticated()) {
          event.preventDefault();
          $state.transitionTo('home');
        }
        if(toState.data.authenticate && !authService.isAuthenticated()){
          event.preventDefault();
          $rootScope.pageTitle = fromState.title + ' | Proelio';
          $state.transitionTo('login');
        }
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState) {
      $rootScope.classes = {
        html: toState.data.classes.html,
        uiView: toState.data.classes.uiView
      };
      $rootScope.wizard = toState.data.wizard;
    });

  }

})();
