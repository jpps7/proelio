/*
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.user.home')
      .controller('HomeCtrl', HomeCtrl);
  /** @ngInject */
  function HomeCtrl($scope, homeService) {

    $scope.loadingData = {
      status: false,
      success: false
    };

    $scope.pics = [];
    // $scope.pics = [
    //   {id: '1', title: 'Lienzo #1', src: 'http://lorempixel.com/300/400/'},
    //   {id: '2', title: 'Lienzo #2', src: 'http://lorempixel.com/300/400/'},
    //   {id: '3', title: 'Lienzo #3', src: 'http://lorempixel.com/200/400/'},
    //   {id: '4', title: 'Lienzo #4', src: 'http://lorempixel.com/300/300/'},
    //   {id: '5', title: 'Lienzo #5', src: 'http://lorempixel.com/600/400/'},
    //   {id: '6', title: 'Lienzo #6', src: 'http://lorempixel.com/200/600/'},
    //   {id: '7', title: 'Lienzo #7', src: 'http://lorempixel.com/300/500/'},
    //   {id: '8', title: 'Lienzo #8', src: 'http://lorempixel.com/300/300/'},
    //   {id: '9', title: 'Lienzo #9', src: 'http://lorempixel.com/400/400/'},
    //   {id: '10', title: 'Lienzo #10', src: 'http://lorempixel.com/600/400/'},
    //   {id: '11', title: 'Lienzo #11', src: 'http://lorempixel.com/250/200/'},
    //   {id: '12', title: 'Lienzo #12', src: 'http://lorempixel.com/600/500/'},
    //   {id: '13', title: 'Lienzo #13', src: 'http://lorempixel.com/300/300/'},
    //   {id: '14', title: 'Lienzo #14', src: 'http://lorempixel.com/500/400/'}
    // ];

    $scope.$on('home:loaded:success', function(event, canvases) {
      $scope.loadingData = {
        status: false,
        success: true
      };
      $scope.pics = canvases;
    });

    $scope.$on('home:loaded:failure', function() {
      $scope.loadingData = {
        status: false,
        success: false
      };
    });

    $scope.$on('home:loading', function() {
      $scope.loadingData = {
        status: true,
        success: false
      };
    });

    $scope.refresh = function(){
      angularGridInstance.gallery.refresh();
    }

    // $scope.loadingTableData =  {
    //   success: false,
    //   status: false
    // };
    // $scope.tableData2 = [];
    // var newUser = false;
    //
    // var fetchTableData = function() {
    //   $scope.loadingTableData.status = true;
    //   Restangular.one('users').get().then(
    //     function(response) {
    //       $scope.tableData = response;
    //       $scope.loadingTableData.success = true;
    //       $scope.loadingTableData.status = false;
    //     },
    //     function(response) {
    //       $scope.tableData = [];
    //       $scope.loadingTableData.status = false;
    //       toastr.error('No se puede conectar con el servidor', 'Error', {
    //         "autoDismiss": false,
    //         "positionClass": "toast-top-right",
    //         "type": "error",
    //         "timeOut": "5000",
    //         "extendedTimeOut": "2000",
    //         "allowHtml": false,
    //         "closeButton": false,
    //         "tapToDismiss": true,
    //         "progressBar": false,
    //         "newestOnTop": true,
    //         "maxOpened": 0,
    //         "preventDuplicates": false,
    //         "preventOpenDuplicates": false
    //       })
    //   });
    // };
    //
    // $scope.roles = [
    //   {value: 1, text: 'Admin'},
    //   {value: 2, text: 'User'}
    // ];
    //
    // $scope.statuses = [
    //   {value: 1, text: 'Active'},
    //   {value: 2, text: 'Inactive'}
    // ];
    //
    // $scope.showStatus = function(user) {
    //   var selected = [];
    //   if(user.status) {
    //     selected = $filter('filter')($scope.statuses, {value: user.status});
    //   }
    //   return selected.length ? selected[0].text : null;
    // };
    //
    // $scope.showRole = function(user) {
    //   var selected = [];
    //   if(user.role) {
    //     selected = $filter('filter')($scope.roles, {value: user.role});
    //   }
    //   return selected.length ? selected[0].text : null;
    // };
    //
    // $scope.addUser = function() {
    //   $scope.inserted = {
    //     fullName: '',
    //     nickname: '',
    //     role: 2,
    //     email: '',
    //     status: 2,
    //   };
    //   $scope.tableData.push($scope.inserted);
    //   newUser = true;
    // };
    //
    // $scope.updateUser = function(data, id) {
    //   if (newUser) {
    //     Restangular.all('users').post(data).then(function() {
    //       newUser = false;
    //     });
    //   } else {
    //     Restangular.one('users', id).patch(data);
    //   }
    // };
    //
    // $scope.removeUser = function(user) {
    //   if (newUser) {
    //     var index = $scope.tableData.indexOf(user);
    //     if (index > -1) $scope.tableData.splice(index, 1);
    //   } else {
    //     Restangular.one('users', user.id).remove().then(function() {
    //       var index = $scope.tableData.indexOf(user);
    //       if (index > -1) $scope.tableData.splice(index, 1);
    //     });
    //   }
    // };
    //
    // // $scope.cancelRow = function(index) {
    // //   if (newUser) {
    // //     if (index > -1) {
    // //       $scope.tableData.splice(index, 1);
    // //       newUser = false;
    // //     }
    // //   } else {
    // //     $scope.rowform.$cancel();
    // //   }
    // // }
    //
    // fetchTableData();
    homeService.get(1);

  }

})();
