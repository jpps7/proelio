/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.user.home', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {

  }

})();
