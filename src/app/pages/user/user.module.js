/*
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.user', [
    'angularGrid',
    'Proelio.pages.user.home'
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {

  }

})();
