/*
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.user')
      .controller('UserCtrl', UserCtrl);
      
  /** @ngInject */
  function UserCtrl($scope) {
    $scope.loadingData = {
      status: false,
      success: false
    };
  }
})();
