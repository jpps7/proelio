/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .controller('EditGroupModalCtrl', EditGroupModalCtrl);

  /** @ngInject */
  function EditGroupModalCtrl($rootScope, $scope, $uibModalInstance, group, groupsService) {
    $scope.group = group;

    $scope.$on('group:edited:success', function() {
      $uibModalInstance.dismiss();
    });

    $scope.$on('group:edited:failure', function() {
      $uibModalInstance.dismiss();
    });

    $scope.update = function(group) {
      groupsService.edit(group);
    };
  }
})();
