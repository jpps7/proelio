/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    // $stateProvider
    //     .state('groups', {
    //       url: '/groups',
    //       templateUrl: 'app/pages/dashboard/groups/groups.html',
    //       title: 'Grupos',
    //       controller: 'GroupsCtrl',
    //       parent: 'dashboard',
    //     });
  }

})();
