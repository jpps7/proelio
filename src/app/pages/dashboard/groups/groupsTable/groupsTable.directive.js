/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .directive('groupsTable', groupsTable);

  /** @ngInject */
  function groupsTable() {
    return {
      restrict: 'AE',
      controller: 'GroupsTableCtrl',
      templateUrl: 'app/pages/dashboard/groups/groupsTable/groupsTable.html'
    };
  }
})();
