/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .controller('GroupsTableCtrl', GroupsTableCtrl);

  /** @ngInject */
  function GroupsTableCtrl($scope, $filter, $state, $uibModal, statusesService, groupsService) {
    $scope.defaultStatuses = [];
    $scope.tableData = [];
    $scope.tableRowSize = 5;

    $scope.$on('statuses:loaded:success', function(event, statuses) {
      $scope.defaultStatuses = statuses;
    });

    $scope.$on('groups:loaded:success', function(event, groups) {
      $scope.$parent.loadingData = {
        status: false,
        success: true
      };
      $scope.safeTableData = groups;
    });

    $scope.$on('groups:loaded:failure', function() {
      $scope.$parent.loadingData = {
        status: false,
        success: false
      };
    });

    $scope.$on('groups:loading', function() {
      $scope.$parent.loadingData = {
        status: true,
        success: false
      };
    });

    $scope.add = function() {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/dashboard/groups/addGroupModal/addGroupModal.html',
        controller: 'AddGroupModalCtrl',
        size: 'lg'
      });
    };

    $scope.remove = function(group) {
      groupsService.delete(group);
    };

    $scope.showStatus = function(group) {
      var selected = [];
      if(group.status) {
        selected = $filter('filter')($scope.defaultStatuses, {id: group.status.id});
      }
      return selected.length ? selected[0].name : null;
    };

    $scope.edit = function(group) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/dashboard/groups/editGroupModal/editGroupModal.html',
        controller: 'EditGroupModalCtrl',
        resolve: {
          group: function() {
            return group;
          }
        },
        size: 'lg'
      });
    };

    groupsService.get();
    statusesService.get();
  }
})();
