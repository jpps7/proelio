/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .directive('membersTable', membersTable);

  /** @ngInject */
  function membersTable() {
    return {
      restrict: 'AE',
      controller: 'MembersTableCtrl',
      templateUrl: 'app/pages/dashboard/groups/members/membersTable/membersTable.html'
    };
  }
})();
