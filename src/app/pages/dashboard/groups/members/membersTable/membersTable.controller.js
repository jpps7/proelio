/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .controller('MembersTableCtrl', MembersTableCtrl);

  /** @ngInject */
  function MembersTableCtrl($scope, $filter, $state, $uibModal, statusesService, rolesService, membersService) {
    $scope.defaultRoles = [];
    $scope.defaultStatuses = [];
    $scope.tableData = [];
    $scope.tableRowSize = 5;

    $scope.$on('roles:loaded:success', function(event, roles) {
      $scope.defaultRoles = roles;
    });

    $scope.$on('statuses_user:loaded:success', function(event, statuses) {
      $scope.defaultStatuses = statuses;
    });

    $scope.$on('members:loaded:success', function(event, members) {
      $scope.$parent.loadingData = {
        status: false,
        success: true
      };
      $scope.safeTableData = members;
    });

    $scope.$on('members:loaded:failure', function() {
      $scope.$parent.loadingData = {
        status: false,
        success: false
      };
    });

    $scope.$on('members:loading', function() {
      $scope.$parent.loadingData = {
        status: true,
        success: false
      };
    });

    $scope.add = function() {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/dashboard/groups/members/addMemberModal/addMemberModal.html',
        controller: 'AddMemberModalCtrl',
        size: 'lg',
        resolve: {
          groupId: function() {
            return $state.params.groupId;
          }
        }
      });
    };

    $scope.remove = function(member) {
      membersService.delete(member);
    };

    $scope.showRole = function(user) {
      var selected = [];
      if(user.role) {
        selected = $filter('filter')($scope.defaultRoles, {id: user.role.id});
      }
      return selected.length ? selected[0].name : null;
    };

    $scope.showStatus = function(group) {
      var selected = [];
      if(group.status) {
        selected = $filter('filter')($scope.defaultStatuses, {id: group.status.id});
      }
      return selected.length ? selected[0].name : null;
    };

    membersService.get($state.params.groupId);
    rolesService.get();
    statusesService.get('users');
  }
})();
