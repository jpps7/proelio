/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .controller('AddMemberModalCtrl', AddMemberModalCtrl);

  /** @ngInject */
  function AddMemberModalCtrl($rootScope, $scope, $state, $uibModalInstance, groupId, availableUsersService, membersService) {
    $scope.availableUsers = [];

    $scope.$on('available_users:loaded:success', function(event, users) {
      $scope.availableUsers = users;
    });

    $scope.$on('member:added:success', function() {
        $uibModalInstance.dismiss();
        // membersService.get(groupId);
    });

    $scope.$on('member:added:failure', function() {
        $uibModalInstance.dismiss();
    });

    $scope.save = function(member) {
      console.log(member);
      membersService.add(member);
    };

    availableUsersService.get(groupId);
  }
})();
