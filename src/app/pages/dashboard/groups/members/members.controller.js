/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .controller('DashboardMembersCtrl', DashboardMembersCtrl);

  /** @ngInject */
  function DashboardMembersCtrl($scope) {
    $scope.loadingData = {
      status: false,
      success: false
    };
  }
})();
