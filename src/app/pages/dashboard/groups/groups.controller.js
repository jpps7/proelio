/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .controller('DashboardGroupsCtrl', DashboardGroupsCtrl);

  /** @ngInject */
  function DashboardGroupsCtrl($scope) {
    $scope.loadingData = {
      status: false,
      success: false
    };
  }
})();
