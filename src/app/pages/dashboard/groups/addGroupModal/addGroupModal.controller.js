/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.groups')
      .controller('AddGroupModalCtrl', AddGroupModalCtrl);

  /** @ngInject */
  function AddGroupModalCtrl($rootScope, $scope, $uibModalInstance, groupsService) {
    $scope.group = {
      name: ''
    };

    $scope.$on('group:added:success', function() {
        $uibModalInstance.dismiss();
        // groupsService.get();
    });

    $scope.$on('group:added:failure', function() {
        $uibModalInstance.dismiss();
    });

    $scope.save = function(group) {
      groupsService.add(group);
    };
  }
})();
