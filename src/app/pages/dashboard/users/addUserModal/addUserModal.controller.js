/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.users')
      .controller('AddUserModalCtrl', AddUserModalCtrl);

  /** @ngInject */
  function AddUserModalCtrl($rootScope, $scope, $uibModalInstance, roles, usersService) {
    $scope.roles = roles;

    $scope.user = {
      fullName: '',
      nickname: '',
      email: '',
      role: {
        id: '1',
        name: 'Administrator'
      }
    }

    $scope.$on('user:added:success', function() {
        $uibModalInstance.dismiss();
        // usersService.get();
    });

    $scope.$on('user:added:failure', function() {
        $uibModalInstance.dismiss();
    });

    $scope.save = function(user) {
      usersService.add(user);
    };
  }
})();
