/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.users')
      .controller('EditUserModalCtrl', EditUserModalCtrl);

  /** @ngInject */
  function EditUserModalCtrl($rootScope, $scope, $uibModalInstance, user, roles, usersService) {
    $scope.user = user;
    $scope.roles = roles;

    $scope.$on('user:edited:success', function() {
      $uibModalInstance.dismiss();
    });

    $scope.$on('user:edited:failure', function() {
      $uibModalInstance.dismiss();
    });

    $scope.update = function(user) {
      usersService.edit(user);
    };
  }
})();
