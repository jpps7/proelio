/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.users')
      .directive('usersTable', usersTable);

  /** @ngInject */
  function usersTable() {
    return {
      restrict: 'AE',
      controller: 'UsersTableCtrl',
      templateUrl: 'app/pages/dashboard/users/usersTable/usersTable.html'
    };
  }
})();
