/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.users')
      .controller('UsersTableCtrl', UsersTableCtrl);

  /** @ngInject */
  function UsersTableCtrl($scope, $filter, $uibModal, rolesService, statusesService, usersService) {
    $scope.defaultRoles = [];
    $scope.defaultStatuses = [];
    $scope.tableData = [];
    $scope.tableRowSize = 5;

    $scope.$on('roles:loaded:success', function(event, roles) {
      $scope.defaultRoles = roles;
    });

    $scope.$on('statuses_user:loaded:success', function(event, statuses) {
      $scope.defaultStatuses = statuses;
    });

    $scope.$on('users:loaded:success', function(event, users) {
      $scope.$parent.loadingData = {
        status: false,
        success: true
      };
      $scope.safeTableData = users;
    });

    $scope.$on('users:loaded:failure', function() {
      $scope.$parent.loadingData = {
        status: false,
        success: false
      };
    });

    $scope.$on('users:loading', function() {
      $scope.$parent.loadingData = {
        status: true,
        success: false
      };
    });

    $scope.showStatus = function(user) {
      var selected = [];
      if(user.status) {
        selected = $filter('filter')($scope.defaultStatuses, {id: user.status.id});
      }
      return selected.length ? selected[0].name : null;
    };

    $scope.showRole = function(user) {
      var selected = [];
      if(user.role) {
        selected = $filter('filter')($scope.defaultRoles, {id: user.role.id});
      }
      return selected.length ? selected[0].name : null;
    };

    $scope.add = function() {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/dashboard/users/addUserModal/addUserModal.html',
        controller: 'AddUserModalCtrl',
        resolve: {
          roles: function() {
            return $scope.defaultRoles;
          }
        },
        size: 'lg'
      });
    };

    $scope.remove = function(user) {
      usersService.delete(user);
    };

    $scope.edit = function(user) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/dashboard/users/editUserModal/editUserModal.html',
        controller: 'EditUserModalCtrl',
        resolve: {
          user: function() {
            return user;
          },
          roles: function() {
            return $scope.defaultRoles;
          }
        },
        size: 'lg'
      });
    };

    usersService.get();
    rolesService.get();
    statusesService.get('users');
  }
})();
