/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.users')
      .controller('DashboardUsersCtrl', DashboardUsersCtrl);

  /** @ngInject */
  function DashboardUsersCtrl($scope) {
    $scope.loadingData = {
      status: false,
      success: false
    };
  }
})();
