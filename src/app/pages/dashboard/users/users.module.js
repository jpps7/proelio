/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.users', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    // $stateProvider
    //     .state('users', {
    //       url: '/users',
    //       templateUrl: 'app/pages/dashboard/users/users.html',
    //       title: 'Usuarios',
    //       controller: 'UsersCtrl',
    //       parent: 'dashboard'
    //     });
  }

})();
