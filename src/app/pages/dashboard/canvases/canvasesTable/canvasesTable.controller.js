/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.canvases')
      .controller('CanvasesTableCtrl', CanvasesTableCtrl);

  /** @ngInject */
  function CanvasesTableCtrl($scope, $filter, $state, $uibModal, canvasesService) {
    $scope.tableData = [];
    $scope.tableRowSize = 5;

    $scope.$on('canvases:loaded:success', function(event, canvases) {
      $scope.$parent.loadingData = {
        status: false,
        success: true
      };
      $scope.safeTableData = canvases;
    });

    $scope.$on('canvases:loaded:failure', function() {
      $scope.$parent.loadingData = {
        status: false,
        success: false
      };
    });

    $scope.$on('canvases:loading', function() {
      $scope.$parent.loadingData = {
        status: true,
        success: false
      };
    });

    $scope.showTags = function(tags) {
      var arrayTags = [];
      angular.forEach(tags, function(value, key) {
        this.push(value.name);
      }, arrayTags);
      return arrayTags.join(', ');
    };

    $scope.remove = function(canvas) {
      canvasesService.delete(canvas);
    };

    canvasesService.get();
  }
})();
