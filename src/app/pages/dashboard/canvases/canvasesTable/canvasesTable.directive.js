/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.canvases')
      .directive('canvasesTable', canvasesTable);

  /** @ngInject */
  function canvasesTable() {
    return {
      restrict: 'AE',
      controller: 'CanvasesTableCtrl',
      templateUrl: 'app/pages/dashboard/canvases/canvasesTable/canvasesTable.html'
    };
  }
})();
