/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.canvases')
      .controller('DashboardCanvasesCtrl', DashboardCanvasesCtrl);

  /** @ngInject */
  function DashboardCanvasesCtrl($scope) {
    $scope.loadingTableData =  {
      success: false,
      status: false
    };
  }
})();
