/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard', [
    'Proelio.pages.dashboard.home',
    'Proelio.pages.dashboard.users',
    'Proelio.pages.dashboard.groups',
    'Proelio.pages.dashboard.tags',
    'Proelio.pages.dashboard.canvases',
    'Proelio.pages.dashboard.profile'
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    // $stateProvider
    //     .state('dashboard', {
    //       url: '/dashboard',
    //       templateUrl: 'app/pages/dashboard/dashboard.html',
    //       title: 'Dashboard',
    //       abstract: 'true',
    //       data: {
    //         authenticate: true,
    //         classes: {
    //           html: 'dashboard-layout',
    //           uiView: 'dashboard-main'
    //         }
    //       }
    //     });
  }

})();
