/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.home', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    // $stateProvider
    //     .state('home', {
    //       url: '/home',
    //       templateUrl: 'app/pages/dashboard/home/home.html',
    //       title: 'Home',
    //       parent: 'dashboard'
    //     });
  }

})();
