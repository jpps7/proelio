/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.home')
      .service('dashboardCalendar', dashboardCalendar);

  /** @ngInject */
  function dashboardCalendar() {

  }
})();
