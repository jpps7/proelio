/*
@author Juan Pablo Pinto Santana
@company Jumay
@created on 11.07.2016
*/
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.home')
      .controller('DashboardHomeCtrl', DashboardHomeCtrl);

  /** @ngInject */
  function DashboardHomeCtrl($scope) {
  }
})();
