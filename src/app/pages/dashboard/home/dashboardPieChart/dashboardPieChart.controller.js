/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.home')
      .controller('DashboardPieChartCtrl', DashboardPieChartCtrl);

  /** @ngInject */
  function DashboardPieChartCtrl($scope, $timeout, baUtil, baConfig, proelioResource, handleResponse) {
    var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);

    // function getRandomArbitrary(min, max) {
    //   return Math.random() * (max - min) + min;
    // }

    var route = 'dashboard';
    $scope.loadingData = {
      status: false,
      success: false
    };


    var loadPieChartsDataFunctions = {
      success: function(response) {
        $scope.charts = handleResponse.success(response);
        $scope.loadingData.success = true;
      },
      error: function(response) {
        handleResponse.error(response);
        $scope.loadingData.success = false;
      },
      final: function() {
        $scope.loadingData.status = false;
      }
    };

    function loadPieCharts(path, success, error, final) {
      proelioResource.get(path, _, success, error, final);
      $('.chart').each(function () {
        var chart = $(this);
        chart.easyPieChart({
          easing: 'easeOutBounce',
          onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          },
          barColor: pieColor,
          trackColor: 'rgba(0,0,0,0)',
          size: 84,
          scaleLength: 0,
          animation: 2000,
          lineWidth: 9,
          lineCap: 'round',
        });
      });

      // $('.refresh-data').on('click', function () {
      //   updatePieCharts();
      // });
    }
    //
    // function updatePieCharts() {
    //   $('.pie-charts .chart').each(function(index, chart) {
    //     $(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
    //   });
    // }

    $timeout(function () {
      loadPieCharts(route, loadPieChartsDataFunctions.success, loadPieChartsDataFunctions.error, loadPieChartsDataFunctions.final);
      // updatePieCharts();
    }, 1000);
  }
})();
