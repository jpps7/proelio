/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.tags')
      .directive('tagsTable', tagsTable);

  /** @ngInject */
  function tagsTable() {
    return {
      restrict: 'AE',
      controller: 'TagsTableCtrl',
      templateUrl: 'app/pages/dashboard/tags/tagsTable/tagsTable.html'
    };
  }
})();
