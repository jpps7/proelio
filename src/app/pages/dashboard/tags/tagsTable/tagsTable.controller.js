/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.tags')
      .controller('TagsTableCtrl', TagsTableCtrl);

  /** @ngInject */
  function TagsTableCtrl($scope, $filter, $uibModal, tagsService) {
    $scope.tableData = [];
    $scope.tableRowSize = 5;

    $scope.$on('tags:loaded:success', function(event, tags) {
      $scope.$parent.loadingData = {
        status: false,
        success: true
      };
      $scope.safeTableData = tags;
    });

    $scope.$on('tags:loaded:failure', function() {
      $scope.$parent.loadingData = {
        status: false,
        success: false
      };
    });

    $scope.$on('tags:loading', function() {
      $scope.$parent.loadingData = {
        status: true,
        success: false
      };
    });

    $scope.add = function() {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/dashboard/tags/addTagModal/addTagModal.html',
        controller: 'AddTagModalCtrl',
        size: 'lg'
      });
    };

    $scope.remove = function(tag) {
      tagsService.delete(tag);
    };

    $scope.edit = function(tag) {
      $uibModal.open({
        animation: true,
        templateUrl: 'app/pages/dashboard/tags/editTagModal/editTagModal.html',
        controller: 'EditTagModalCtrl',
        resolve: {
          tag: function() {
            return tag;
          }
        },
        size: 'lg'
      });
    };

    tagsService.get();
  }
})();
