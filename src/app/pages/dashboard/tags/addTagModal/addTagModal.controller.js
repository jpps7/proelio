/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.tags')
      .controller('AddTagModalCtrl', AddTagModalCtrl);

  /** @ngInject */
  function AddTagModalCtrl($rootScope, $scope, $uibModalInstance, tagsService) {
    $scope.tag = {
      name: ''
    };

    $scope.$on('tag:added:success', function() {
        $uibModalInstance.dismiss();
        // tagsService.get();
    });

    $scope.$on('tag:added:failure', function() {
        $uibModalInstance.dismiss();
    });

    $scope.save = function(tag) {
      tagsService.add(tag);
    };
  }
})();
