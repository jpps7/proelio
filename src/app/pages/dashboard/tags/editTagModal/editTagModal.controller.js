/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.tags')
      .controller('EditTagModalCtrl', EditTagModalCtrl);

  /** @ngInject */
  function EditTagModalCtrl($rootScope, $scope, $uibModalInstance, tag, tagsService) {
    $scope.tag = tag;

    $scope.$on('tag:edited:success', function() {
      $uibModalInstance.dismiss();
    });

    $scope.$on('tag:edited:failure', function() {
      $uibModalInstance.dismiss();
    });

    $scope.update = function(tag) {
      tagsService.edit(tag);
    };
  }
})();
