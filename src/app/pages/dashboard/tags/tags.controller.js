/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.tags')
      .controller('DashboardTagsCtrl', DashboardTagsCtrl);

  /** @ngInject */
  function DashboardTagsCtrl($scope) {
    $scope.loadingTableData =  {
      success: false,
      status: false
    };
  }
})();
