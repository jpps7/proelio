/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.dashboard.profile', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    // $stateProvider
    //     .state('profile', {
    //       url: '/profile',
    //       title: 'Profile',
    //       templateUrl: 'app/pages/dashboard/profile/profile.html',
    //       controller: 'ProfilePageCtrl',
    //       parent: 'dashboard'
    //     });
  }

})();
