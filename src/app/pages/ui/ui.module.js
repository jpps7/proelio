/**
 * @author k.danovsky
 * created on 12.01.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.ui', [
    'Proelio.pages.ui.typography',
    'Proelio.pages.ui.buttons',
    'Proelio.pages.ui.icons',
    'Proelio.pages.ui.modals',
    'Proelio.pages.ui.grid',
    'Proelio.pages.ui.alerts',
    'Proelio.pages.ui.progressBars',
    'Proelio.pages.ui.notifications',
    'Proelio.pages.ui.tabs',
    'Proelio.pages.ui.slider',
    'Proelio.pages.ui.panels',
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
        .state('ui', {
          url: '/ui',
          template : '<ui-view></ui-view>',
          abstract: true,
          title: 'UI Features',
          sidebarMeta: {
            icon: 'ion-android-laptop',
            order: 200,
          },
        });
  }

})();
