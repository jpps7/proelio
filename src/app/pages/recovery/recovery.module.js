/*
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.recovery', [])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {

  }

})();
