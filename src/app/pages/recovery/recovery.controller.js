/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.recovery')
      .controller('RecoveryCtrl', RecoveryCtrl);

  /** @ngInject */
  function RecoveryCtrl($scope, $state) {

  }
})();
