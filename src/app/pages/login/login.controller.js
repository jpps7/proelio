/**
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages.login')
      .controller('LoginCtrl', LoginCtrl);

  /** @ngInject */
  function LoginCtrl($scope, $state, localStorage, authService) {

    $scope.isLoginButtonDisabled = false;
    $scope.message = '';
    $scope.loginData = localStorage.getObject('userinfo','{}');

    $scope.doLogin = function() {
      this.isLoginButtonDisabled = true;
      this.message = '';
      authService.login(this.loginData);
    }

    $scope.$on('login:successful', function() {
      $state.transitionTo('home');
    });

    $scope.$on('login:unsuccessful', function() {
      $scope.loginData = {
        username: '',
        password: ''
      };
      $scope.loginForm.$setPristine();
      $scope.isLoginButtonDisabled = false;
    });

  }
})();
