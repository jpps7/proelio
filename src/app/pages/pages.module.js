/*
 * @author Juan Pablo Pinto
 * @company Jumay
 * created on 21.06.2016
 */
(function () {
  'use strict';

  angular.module('Proelio.pages', [
    'angularSpinner',
    'Proelio.pages.dashboard',
    'Proelio.pages.user',
    'Proelio.pages.login',
    'Proelio.pages.recovery'    
  ])
      .config(routeConfig);

  /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
    // $urlRouterProvider.otherwise('/dashboard/home');
    //
    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Home',
    //   icon: 'ion-android-home',
    //   stateRef: 'home'
    // });
    //
    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Usuarios',
    //   icon: 'ion-android-person',
    //   stateRef: 'users'
    // });
    //
    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Grupos',
    //   icon: 'ion-android-people',
    //   stateRef: 'groups'
    // });

    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Pages',
    //   icon: 'ion-document',
    //   subMenu: [{
    //     title: 'Sign In',
    //     fixedHref: 'auth.html',
    //     blank: true
    //   }, {
    //     title: 'Sign Up',
    //     fixedHref: 'reg.html',
    //     blank: true
    //   }, {
    //     title: 'User Profile',
    //     stateRef: 'profile'
    //   }, {
    //     title: '404 Page',
    //     fixedHref: '404.html',
    //     blank: true
    //   }]
    // });
    // baSidebarServiceProvider.addStaticItem({
    //   title: 'Menu Level 1',
    //   icon: 'ion-ios-more',
    //   subMenu: [{
    //     title: 'Menu Level 1.1',
    //     disabled: true
    //   }, {
    //     title: 'Menu Level 1.2',
    //     subMenu: [{
    //       title: 'Menu Level 1.2.1',
    //       disabled: true
    //     }]
    //   }]
    // });
  }

})();
