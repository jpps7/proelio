'use strict';

angular.module('Proelio', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',
  'restangular',
  'Proelio.theme',
  'Proelio.pages',
  'Proelio.kitchensink'
]);
